﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Defines an item counter
/// </summary>
[CreateAssetMenu(fileName = "InteractableItemCounter", menuName = "Interactable Item Counter")]
public class InteractableItemCounter : ScriptableObject
{
    /// <summary>
    /// Maximum count. When it is reached, counter is finished. 
    /// </summary>
    [Tooltip("Maximum count. When it is reached, counter is finished. ")]
    public int maxCount;

    /// <summary>
    /// Sticker that is rewarded when counter is finished. 
    /// </summary>
    [Tooltip("Sticker that is rewarded when counter is finished. ")]
    public Sticker rewardSticker;

    /// <summary>
    /// Finished counter. Gives rewardSticker to the player
    /// </summary>
    /// <param name="position">World position where sticker notification will appear</param>
    public void FinishCounter(Vector3 position)
    {
        if (rewardSticker != null)
        {
            if (StickerBook.Instance != null && StickerBook.Instance.AddSticker(rewardSticker))
            {
                // Show sticker gain notification
                StickerNotificationManager.Instance.SpawnNotification(position, rewardSticker);
            }
        }
    }
}
