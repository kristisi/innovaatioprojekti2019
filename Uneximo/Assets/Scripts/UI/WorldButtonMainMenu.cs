﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Functionality of World Selection Buttons located in Main Menu.
/// </summary>
public class WorldButtonMainMenu : MonoBehaviour, IWorldButton
{
    /// <summary>
    /// Reference to the Titlescreen Script. Set in Inspector.
    /// </summary>
    public TitleScreen titlescreen;
    /// <summary>
    /// What island enum does the button represent.
    /// </summary>
    public WorldManager.WorldID island;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnButtonClick);
    }

    /// <summary>
    /// Calls for GameController to load a new island.
    /// </summary>
    /// <param name="islandToLoad">Island type to load (Enum).</param>
    public void LoadWorld(WorldManager.WorldID islandToLoad)
    {
        GameController.Instance.LoadLevel(islandToLoad);
    }

    /// <summary>
    /// Plays tutorial animation if first time, otherwise loads up a new world.
    /// </summary>
    public void OnButtonClick()
    {
        if (AudioManager.Instance != null)
        {
            AudioManager.Instance.PlayButtonSound();
        }

        if (GameController.Instance.miscData.startTutorialCompleted)
        {
            GameController.Instance.OnTitlescreenClosed();
            LoadWorld(island);
            UIController.Instance.UpdateMainWorldButton(island);
            titlescreen.TurnOffTitlescreen();
        }
        else
        {
            titlescreen.TurnOffTitlescreen();
            LoadingScreenTutorialController.Instance.StartTutorial(island);
        }
    }
}
