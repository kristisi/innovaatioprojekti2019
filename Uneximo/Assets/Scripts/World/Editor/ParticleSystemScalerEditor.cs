﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Custom editor that adds a button to the inspector. 
/// The button finds scalable components in the hierarchy. 
/// </summary>
[CustomEditor(typeof(ParticleSystemScaler))]
public class ParticleSystemScalerEditor : Editor
{
    int found = -1;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Find Particle Systems"))
        {
            ParticleSystemScaler scaler = (ParticleSystemScaler) target;
            Undo.RecordObject(target, "Found Particle Systems for ParticleSystemScaler");
            PrefabUtility.RecordPrefabInstancePropertyModifications(target);
            found = scaler.FindParticleSystems();
        }

        if (found == 0)
        {
            EditorGUILayout.HelpBox("Unable to find any ParticleSystems in children of this object's transform.", MessageType.Warning, true);
        }
        else if (found > 0)
        {
            EditorGUILayout.HelpBox("Found " + found.ToString() + " ParticleSystems in children of this object's transform.", MessageType.None, true);
        }
    }
}
