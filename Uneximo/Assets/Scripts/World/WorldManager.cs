﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls a selection of worlds, changing the current world and its activity status and assigning WorldNavigator instance to
/// the currently active world.
/// </summary>
public class WorldManager : MonoBehaviour
{
    /// <summary>
    /// A delegate for world activation events.
    /// </summary>
    public delegate void WorldActivationHandler();
    /// <summary>
    /// This event is raised when the currently selected world is activated.
    /// </summary>
    public event WorldActivationHandler WorldActivated;
    /// <summary>
    /// This event is raised when the currently selected world is deactivated.
    /// </summary>
    public event WorldActivationHandler WorldDeactivated;
    /// <summary>
    /// This event is raised when call comes to change world.
    /// </summary>
    public event WorldActivationHandler WorldSelected;

    /// <summary>
    /// A function to raise WorldActivated event.
    /// </summary>
    protected virtual void OnWorldActivated()
    {
        if (WorldActivated != null)
        {
            WorldActivated();
        }
    }
    /// <summary>
    /// A function to raise WorldDeactivated event.
    /// </summary>
    protected virtual void OnWorldDeactivated()
    {
        if (WorldDeactivated != null)
        {
            WorldDeactivated();
        }
    }
    /// <summary>
    /// A function to raise WorldSelected event.
    /// </summary>
    protected virtual void OnWorldSelected()
    {
        if (WorldSelected != null)
        {
            WorldSelected();
        }
    }
    /// <summary>
    /// A singleton instance of WorldManager.
    /// </summary>
    public static WorldManager Instance = null;

    /// <summary>
    /// Neutral = 0, Magic = 1, Mountain = 2, Space = 3, Star = 4.
    /// Add new world IDs to the end. 
    /// Do not change the order of existing values. 
    /// </summary>
    public enum WorldID
    {
        Neutral, Magic, Mountain, Space, Star
    }
    /// <summary>
    /// A list of world gameobject prefabs, index[0] is a dummy. This is used in initializing WorldGameObjects list.
    /// </summary>
    [Tooltip ("Neutral = 0, Magic = 1, Mountain = 2, Space = 3, Star = 4.")]
    public GameObject[] worldPrefabs;
    /// <summary>
    /// A gameobject with WorldNavigator script. It is assigned to the currently selected world gameobject.
    /// </summary>
    public WorldNavigator worldNavigator;
    /// <summary>
    /// A list of selectable world gameobjects. This is list is initialized in Awake() from prefabs in worldPrefabs list.
    /// </summary>

    private GameObject c_world;
    private GameObject n_world;
    private WorldID c_ID = WorldID.Neutral;
    private WorldID n_ID = WorldID.Neutral;

    private bool currentAnchorStatus = false;

    private bool worldChangeWaiting = false;
    private bool interactionsWaiting = false;
    private bool keepHidden = false;
    private bool waitingToHide = false;
    private float maxHide = 3f;
    private float timer = 0f;

    private int activeInteractions = 0;

    private InteractByLooking lookInteract;

    /// <summary>
    /// Lighting settings ambient color for each world. 
    /// </summary>
    public List<WorldAmbientColor> worldAmbientColors;
    private enum State
    {
        idle,
        startAnimation,
        active,
        endAnimation
    }
    private State currentState = State.idle;
    /// <summary>
    /// This is used in setting an ambient color to each WorldID.
    /// </summary>
    [System.Serializable]
    public class WorldAmbientColor
    {
        public WorldID worldID;
        public Color color;
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        ARController.Instance.AnchoredStatusChange += AnchorStatusChanged;
        UIController.Instance.ShowStickerBook += ToggleLookRecticleOff;
        UIController.Instance.HideStickerBook += ToggleLookRecticleOn;
        // UIController.Instance.ShowSettings += ToggleLookRecticleOff;
        // UIController.Instance.HideSettings += ToggleLookRecticleOn;
        lookInteract = FindObjectOfType<InteractByLooking>();
        ToggleLookRecticleOff();
    }

    private void Update()
    {
        if (interactionsWaiting)
        {
            timer += Time.deltaTime;
            if (activeInteractions==0 || timer > maxHide)
            {
                timer = 0f;
                activeInteractions = 0;
                interactionsWaiting = false;
                ChangeActiveWorld(n_ID);
            }
        }
    }

    private void AnchorStatusChanged(bool newStatus)
    {
        currentAnchorStatus = newStatus;
        switch (currentState)
        {
            case (State.idle):
                // no effect
                break;
            case (State.active):
                if (worldChangeWaiting)
                {
                    break;
                }
                if (activeInteractions==0)
                {
                    StartEndAnimation();
                }
                else
                {
                    worldChangeWaiting = true;
                }
                break;
            case (State.startAnimation):
                // no effect
                break;
            case (State.endAnimation):
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// This function activates / deactivates the interaction look recticle gameobject.
    /// </summary>
    /// <param name="status">Is recticle active.</param>
    public void ToggleLookReticle(bool status)
    {
        if (status)
        {
            ToggleLookRecticleOn();
        }
        else
        {
            ToggleLookRecticleOff();
        }
    }

    /// <summary>
    /// Respawns the world.
    /// </summary>
    public void RespawnWorld()
    {
        n_ID = c_ID;
        ChangeActiveWorld(c_ID);
    }

    /// <summary>
    /// This function is called when changing the active world. It starts appropriate processes depending on the
    /// WorldManager state and possible ongoing animations.
    /// </summary>
    /// <param name="worldToLoad"></param>
    public void ChangeActiveWorld(WorldID worldToLoad)
    {
        OnWorldSelected();
        n_ID = worldToLoad;
        //Instantiate the next world before possible ongoing animations have ended
        if (c_ID != n_ID && n_world==null)
        {
            n_world = Instantiate(worldPrefabs[(int)n_ID]);
            n_world.SetActive(false);
        }
        //if there are ongoing interaction animations running, wait and come back from Update
        if (activeInteractions != 0)
        {
            timer = 0f;
            interactionsWaiting = true;
            return;
        }

        if (!keepHidden||waitingToHide)
        {
            switch(currentState)
            {
                case (State.idle):
                    c_world = n_world;
                    c_world.SetActive(true);
                    n_world = null;
                    c_ID = n_ID;

                    ChangeWorldAmbientColor(worldToLoad);
                    currentState = State.startAnimation;
                    worldNavigator.gameObject.SetActive(true);
                    GameController.Instance.OnWorldChangeStarted(worldToLoad);
                    worldNavigator.Init(c_world.transform, currentAnchorStatus);
                    break;

                case (State.active):
                    StartEndAnimation();
                    break;

                case (State.startAnimation):
                    worldChangeWaiting = true;
                    break;
                case (State.endAnimation):
                    break;
            }
        }
    }

    private void ShowWorld()
    {
        keepHidden = false;
        ChangeActiveWorld(n_ID);
    }

    private void HideWorld()
    {
        keepHidden = true;
        waitingToHide = true;
        ChangeActiveWorld(c_ID);
    }

    private void StartEndAnimation()
    {
        OnWorldDeactivated();
        GameController.Instance.OnWorldChangeStarted(n_ID);
        currentState = State.endAnimation;
        ToggleLookReticle(false);
        worldNavigator.StartEndAnimation();
    }
    /// <summary>
    /// WorldNavigator calls this function when it has finished an end animation. If world is meant to stay hidden,
    /// this function deactivates the current world and sets WorldManager's state as idle; else it sets state as startAnimation
    /// and calls WorldNavigator's Init function to spawn the world.
    /// </summary>
    public void EndAnimationOver()
    {
        ChangeWorldAmbientColor(n_ID);
        if (c_world && c_ID != n_ID)
        {
            DestroyImmediate(c_world);
        }
        if (waitingToHide)
        {
            waitingToHide = false;
            currentState = State.idle;
            if (c_world)
            {
                DestroyImmediate(c_world);
                c_world = null;
                n_ID = c_ID;
                c_ID = WorldID.Neutral;

            }
        }
        else
        {
            if (c_ID!=n_ID)
            {
                c_world = n_world;
                n_world = null;
            }
            c_world.SetActive(true);
            currentState = State.startAnimation;
            c_ID = n_ID;
            worldNavigator.Init(c_world.transform, currentAnchorStatus);
        }
    }
    /// <summary>
    /// WorldNavigator calls this function when start animation is finished. If there is a new world change waiting,
    /// this process will be started - else WorldManager's current state is set as active.
    /// </summary>
    public void StartAnimationOver()
    {
        if (worldChangeWaiting)
        {
            worldChangeWaiting = false;
            currentState = State.endAnimation;
            worldNavigator.StartEndAnimation();
            return;
        }
        currentState = State.active;
        if (currentAnchorStatus)
        {
            ToggleLookReticle(true);
        }
        OnWorldActivated();
    }

    /// <summary>
    /// Returns the currently selected world.
    /// </summary>
    /// <returns></returns>
    public GameObject GetWorld()
    {
        return c_world;
    }

    private void ChangeWorldAmbientColor(WorldID worldID)
    {
        if (worldAmbientColors != null && worldAmbientColors.Count > 0)
        {
            foreach(WorldAmbientColor worldAmbientColor in worldAmbientColors)
            {
                if (worldAmbientColor.worldID == worldID)
                {
                    RenderSettings.ambientLight = worldAmbientColor.color;
                    return;
                }
            }
        }

        Debug.LogWarning("No color set for world " + System.Enum.GetName(typeof(WorldID), worldID) + " in WorldManager.");
    }
    /// <summary>
    /// InteractableItem calls this function to add to the count of ongoing interaction events when animation starts.
    /// </summary>
    public void InteractionStarted()
    {
        activeInteractions++;
    }
    /// <summary>
    /// InteractableItem calls this function to decrease from the count of ongoing interaction events when animation has ended.
    /// </summary>
    public void InteractionEnded()
    {
        activeInteractions--;
    }
    /// <summary>
    /// Toggles the interaction look recticle off.
    /// </summary>
    public void ToggleLookRecticleOff()
    {
        lookInteract.DisableInteraction();
    }
    /// <summary>
    /// Toggles the interaction look recticle on.
    /// </summary>
    public void ToggleLookRecticleOn()
    {
        if (currentAnchorStatus == true)
        {
            lookInteract.EnableInteraction();
        }
    }
    
}
