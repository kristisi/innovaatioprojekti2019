﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// WorldNavigator is attached to the currently selected world gameobject by WorldManager. It handles start and end
/// animations and listens player input events to world gameobjects scale, position and rotation.
/// </summary>
public class WorldNavigator : MonoBehaviour
{
    /// <summary>
    /// Transform of the currently selected world gameobject.
    /// </summary>
    public Transform worldTransform;
    /// <summary>
    /// Multiplier for rotation touch sensitivity.
    /// </summary>
    public float rotateSpeed = 0.08f;
    private float minScale = 0.1f;
    private float maxScale = 1.5f;

    private Vector3 originalScale;
    /// <summary>
    /// The current uniform scale value of the world gameobject.
    /// </summary>
    public float currentScale = 0.5f;
    private float defaultScale = 0.5f;
    private float zoomSpeed = 0.001f;
    private bool pinchIsOn = false;

    private Stack<float> rotationValues;
    private Vector3 worldPosition;
    private int rotationStackSize = 10;
    private float moment = 0f;
    private bool movingClockwise = false;
    /// <summary>
    /// This value is used to adjust how fast the rotation declerates when swipe touch has ended. A sensible value is
    /// above 0.9f.
    /// </summary>
    [Range(0.7f, 0.9999f)]
    public float slowDownMultiplier = 0.95f;
    private bool startAnimationOn = false;
    private bool endAnimationOn = false;
    private bool worldActivated = false;
    /// <summary>
    /// An animation curve of the world scaling up when spawned.
    /// </summary>
    public AnimationCurve scaleUpCurve;
    /// <summary>
    /// An animation curve of the world scaling down when closing.
    /// </summary>
    public AnimationCurve scaleDownCurve;
    private float curveTime = 0f;
    /// <summary>
    /// A shared speed multiplier for start and end aimation.
    /// </summary>
    public float curveSpeed = 1.2f;

    private bool anchored = false;
    /// <summary>
    /// A distance from the camera that is used to place the world.
    /// </summary>
    public float fixedDistance = 1f;
    /// <summary>
    /// In anchored mode, when using height relative to the tilt of the camera, this value is used as a +/- height
    /// difference limitter between the camera and the placed world object.
    /// </summary>
    public float maxSetPositionHeightDifference = 0.3f;
    /// <summary>
    /// This value limits how mutch the world placement height can be adjusted up and down.
    /// </summary>
    public float heightLimit = 0.7f;
    private float currentHeight = 0f;
    private Quaternion currentRotation = Quaternion.identity;
    private float heightSensitivity = 0.001f;
    private float heightThreshold = 30f;
    private Vector3 originalGravity;
    /// <summary>
    /// Selectable mode in anchored mode, default is false. If true, places the world at the equal height with the camera.
    /// </summary>
    public bool usingCameraHeight = false;

    // USED ONLY IN PC BUILD
    //private float pcBuildSpeed = 1.5f;

    private void Start()
    {
        PlayerInput.Instance.TouchMoved += RotateWorld;
        PlayerInput.Instance.PinchTouch += ScaleWorld;
        PlayerInput.Instance.PinchStatusChange += PinchStatusChanged;
        PlayerInput.Instance.TouchEnded += TouchHaveEnded;
        PlayerInput.Instance.TouchBegan += StopRotation;
        rotationValues = new Stack<float>();

        originalGravity = Physics.gravity;
        currentScale = defaultScale;
    }

    private void Update()
    {
        if (!worldTransform)
        {
            return;
        }
        Vector3 pos;
        if (!anchored)
        {
            pos = Camera.main.transform.position + Camera.main.transform.forward*fixedDistance;
        }
        else
        {
            pos = worldPosition;
        }
        pos.y += currentHeight;
        worldTransform.position = pos;
        if (endAnimationOn)
        {
            DoEndAnimation();
            return;
        }
        if (startAnimationOn)
        {
            DoStartAnimation();
            return;
        }
        Vector3 dir = worldTransform.position - Camera.main.transform.position;
        dir.y = Camera.main.transform.position.y;
        dir.Normalize();
        pos = worldTransform.position;
        pos.y = Camera.main.transform.position.y;
        float behindCamera = Vector3.Dot(dir, Camera.main.transform.forward);
        float dist = Vector3.Distance(pos, Camera.main.transform.position);
        if (behindCamera < 0.3f && dist > 3.0f || dist > 5f)
        {
            WorldManager.Instance.RespawnWorld();
        }

        if (moment != 0f)
        {
            worldTransform.Rotate(Vector3.up, moment * rotateSpeed);
            moment *= slowDownMultiplier;
            switch (movingClockwise)
            {
                // moment is clockwise (positive)
                case (true):
                    if (moment < 0.1f)
                    {
                        moment = 0f;
                    }
                    break;
                    // moment is anticlockwise (negative)
                case (false):
                    if (moment > -0.1f)
                    {
                        moment = 0f;
                    }
                    break;
            }
        }
        #region PC BULD CONTROLS
        /*
        if (Input.GetKey(KeyCode.W))
        {
            worldTransform.Rotate(Vector3.left, pcBuildSpeed);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            worldTransform.Rotate(Vector3.right, pcBuildSpeed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            worldTransform.Rotate(Vector3.up, pcBuildSpeed);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            worldTransform.Rotate(Vector3.down, pcBuildSpeed);
        }
        if (Input.GetKey(KeyCode.Q))
        {
            worldTransform.Rotate(Vector3.forward, pcBuildSpeed);
        }
        else if (Input.GetKey(KeyCode.E))
        {
            worldTransform.Rotate(Vector3.back, pcBuildSpeed);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            worldTransform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
        } 
        */
        #endregion
    }
    /// <summary>
    /// WorldManager calls this initialization function when spawning the world. 
    /// </summary>
    /// <param name="world">Transform of the current world gameobject.</param>
    /// <param name="anchoredStatus">The anchor status of the ARController</param>
    public void Init(Transform world, bool anchoredStatus)
    {
        worldTransform = world;
        originalScale = worldTransform.localScale;
        anchored = anchoredStatus;
        startAnimationOn = true;
        worldActivated = false;
        curveTime = 0f;
        worldTransform.rotation = currentRotation;

        if (anchored)
        {
            SetWorldPosition();
        }
        SetCurrentScale();
    }

    private void DoStartAnimation()
    {
        curveTime += Time.deltaTime * curveSpeed;
        if (curveTime < 1f)
        {
            float scale = scaleUpCurve.Evaluate(curveTime)*currentScale;
            worldTransform.localScale = originalScale * scale;
            if (!worldActivated)
            {
                worldActivated = true;
                worldTransform.gameObject.SetActive(true);
            }

            ScaleComponents(scale);
        }
        else
        {
            curveTime = 0f;
            startAnimationOn = false;
            WorldManager.Instance.StartAnimationOver();
        }
    }

    private void DoEndAnimation()
    {
        currentRotation = worldTransform.rotation;
        curveTime += Time.deltaTime * curveSpeed;
        if (curveTime < 1f)
        {
            float scale = scaleDownCurve.Evaluate(curveTime) * currentScale;
            worldTransform.localScale = originalScale * scale;
            
            ScaleComponents(scale);
        }
        else
        {
            curveTime = 0f;
            endAnimationOn = false;
            worldTransform.gameObject.SetActive(false);
            ResetModelScale();
            WorldManager.Instance.EndAnimationOver();
        }
    }
    /// <summary>
    /// Starts end animation.
    /// </summary>
    public void StartEndAnimation()
    {
        endAnimationOn = true;
        curveTime = 0f;

    }
    
    private void SetWorldPosition()
    {
        if (anchored)
        {
            Vector3 cameraForward = Camera.main.transform.forward;
            Vector3 cameraBearing = new Vector3(cameraForward.x, 0f, cameraForward.z).normalized;

            if (usingCameraHeight)
            {
                worldPosition = cameraBearing * fixedDistance + Camera.main.transform.position;
                worldTransform.position = worldPosition;
            }
            else
            {
                Vector3 vec = cameraForward * fixedDistance;
                if (vec.y > maxSetPositionHeightDifference)
                {
                    vec.y = maxSetPositionHeightDifference;
                }
                else if (vec.y < -maxSetPositionHeightDifference)
                {
                    vec.y = -maxSetPositionHeightDifference;
                }
                worldPosition = vec.normalized * fixedDistance;
            }
            worldTransform.rotation = Quaternion.LookRotation(cameraBearing, Vector3.up);
        }
    }

    private void SetCurrentScale()
    {
        worldTransform.localScale = originalScale * currentScale;
        ScaleComponents(currentScale);
    }
    /// <summary>
    /// Scales worlds geometry and call's function to scale related scalable components.
    /// </summary>
    /// <param name="scale"></param>
    public void ScaleWorld(float scale)
    {
        if (worldTransform == null || startAnimationOn ||endAnimationOn)
        {
            return;
        }
        currentScale = Mathf.Clamp(currentScale + scale * zoomSpeed, minScale, maxScale);
        worldTransform.localScale = originalScale * currentScale;

        ScaleComponents(currentScale);
    }

    /// <summary>
    /// Finds and calls each of worldTransform's scalable IComponentScaler components to scale accordingly.
    /// </summary>
    private void ScaleComponents(float scale)
    {
        IComponentScaler[] scalers = worldTransform.GetComponents<IComponentScaler>();

        if (scalers != null && scalers.Length > 0)
        {
            foreach(IComponentScaler scaler in scalers)
            {
                scaler.Scale(scale);
            }
        }

        // Not required as gravitymodifier is scaled in each particle system
        // If other gravity based objects are added, consider this again, 
        // but be careful of not scaling for example particle gravity twice
        // Check ParticleSystemScaler
        // Physics.gravity = originalGravity * currentScale;
    }
    /// <summary>
    /// When end animation is has ended, this function is called to reset the scale of the world transform.
    /// </summary>
    private void ResetModelScale()
    {
        if (startAnimationOn)
        {
            startAnimationOn = false;
        }

        if (worldTransform != null)
        {
            worldTransform.localScale = originalScale;
            
            ScaleComponents(1f);
        }
    }

    /// <summary>
    /// This function is connected to the rotation player input events.
    /// </summary>
    /// <param name="touch">UnityEngine Touch</param>
    private void RotateWorld(Touch touch)
    {
        if (startAnimationOn)
        {
            return;
        }
        if (worldTransform == null || pinchIsOn)
        {
            rotationValues.Clear();
            moment = 0f;
            return;
        }

        worldTransform.Rotate(Vector3.up, -touch.deltaPosition.x * rotateSpeed);

        rotationValues.Push(-touch.deltaPosition.x);
        if (rotationValues.Count > rotationStackSize)
        {
            rotationValues.Pop();
        }

        if (Mathf.Abs(touch.deltaPosition.x) < heightThreshold)
        {
            AdjustHeight(touch);
        }
    }
    /// <summary>
    /// This event is connected to the height adjusment player input events.
    /// </summary>
    /// <param name="touch">UnityEngine Touch</param>
    private void AdjustHeight(Touch touch)
    {
        currentHeight = Mathf.Clamp(currentHeight + touch.deltaPosition.y * heightSensitivity, -heightLimit, heightLimit);
    }
    /// <summary>
    /// This function is connected to the pinch status change player input events to keep track on the current
    /// touch mode.
    /// </summary>
    /// <param name="status">Is pinch touch now on.</param>
    private void PinchStatusChanged(bool status)
    {
        pinchIsOn = status;
        moment = 0f;
    }
    /// <summary>
    /// This function is connected to touch ended player input events. If there is accumulated rotation moment,
    /// this touch declerating after touch rotation.
    /// </summary>
    /// <param name="touch">UnityEngine Touch</param>
    private void TouchHaveEnded(Touch touch)
    {
        if (rotationValues.Count == 0)
        {
            return;
        }
        int divider = (rotationValues.Count * (rotationValues.Count + 1)) / 2;
        float sum = 0f;
        int multiplier = 1;
        while (rotationValues.Count > 0)
        {
            sum += rotationValues.Pop() * multiplier;
            multiplier++;
        }
        moment = sum / divider;
        if (moment > 0)
        {
            movingClockwise = true;
        }
        else
        {
            movingClockwise = false;
        }
    }
    /// <summary>
    /// Sets rotation moment to zero.
    /// </summary>
    /// <param name="touch">UnityEngine Touch</param>
    private void StopRotation(Touch touch)
    {
        moment = 0f;
    }
}
