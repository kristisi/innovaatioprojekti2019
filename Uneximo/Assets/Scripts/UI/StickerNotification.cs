﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles Sticker Notification logic
/// </summary>
[RequireComponent(typeof(Animator))]
public class StickerNotification : MonoBehaviour
{
    private const string ANIMATOR_IDLE_TRIGGER_KEY = "Idle";
    private const string ANIMATOR_TO_STICKER_BOOK_TRIGGER_KEY = "ToStickerBook";

    // Image used for sticker sprite
    public Image stickerImage;
    public Button stickerButton;

    // Obsolete
    private float flyTime = 1f;

    /// <summary>
    /// How long does it take for the sticker to fly from the source to screen center
    /// </summary>
    public float moveToWaitPositionTime = 1f;

    /// <summary>
    /// How long the sticker waits in the screen center
    /// </summary>
    public float waitTime = 2f;

    private bool collected = false;

    private Vector2 originalSizeDelta;

    private RectTransform rectTransform;

    private Animator animator;

    private void Awake()
    {
        rectTransform = stickerImage.GetComponent<RectTransform>();

        if (rectTransform == null)
        {
            Debug.LogError("StickerNotificationScreenSpace stickerImage must have rectTransform");
            Destroy(gameObject);
        }

        stickerImage.enabled = false;

        animator = GetComponent<Animator>();
        animator.enabled = false;
    }

    /// <summary>
    /// Initializes the notification, setting the sprite and setting initial size to 0
    /// </summary>
    /// <param name="sticker"></param>
    public void Init(Sticker sticker)
    {
        stickerButton.onClick.AddListener(Clicked);
        stickerImage.sprite = sticker.icon;
        originalSizeDelta = rectTransform.sizeDelta;
        rectTransform.sizeDelta = new Vector2(0.001f, 0.001f);
    }

    /// <summary>
    /// Shows the notification
    /// </summary>
    public void Show()
    {
        stickerImage.enabled = true;
    }

    /// <summary>
    /// Begins movement to wait position
    /// </summary>
    /// <param name="waitPos">Position to move to for wait animation</param>
    public void StartAnimation(Vector3 waitPos)
    {
        MoveToWaitPosition(waitPos);

        if (AudioManager.Instance != null)
        {
            AudioManager.Instance.PlayStickerGetSound();
        }
    }
    /// <summary>
    /// Hides sticker image.
    /// </summary>
    public void Hide()
    {
        stickerImage.enabled = false;
    }

    private void MoveToWaitPosition(Vector3 waitPos)
    {
        // Vector3 waitGoal = Camera.main.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        Vector3 waitGoal = waitPos;
        StartCoroutine(MoveToWaitPositionSequence(transform.position, waitGoal));
    }

    IEnumerator MoveToWaitPositionSequence(Vector3 startPos, Vector3 goalPos)
    {
        float timer = 0f;
        while (timer < moveToWaitPositionTime)
        {
            yield return new WaitForEndOfFrame();
            timer += Time.deltaTime;

            float lerpValue = timer / moveToWaitPositionTime;

            transform.position = Vector3.Lerp(startPos, goalPos, lerpValue);

            rectTransform.sizeDelta = Vector2.Lerp(Vector2.zero, originalSizeDelta, lerpValue);
        }

        // Set pos and size to exact goal
        transform.position = goalPos;
        rectTransform.sizeDelta = originalSizeDelta;

        // Trigger idle animation
        animator.enabled = true;
        animator.SetTrigger(ANIMATOR_IDLE_TRIGGER_KEY);

        StartCoroutine(WaitForCollect());
    }

    IEnumerator WaitForCollect()
    {
        float timer = 0f;
        while (timer < waitTime)
        {
            yield return new WaitForEndOfFrame();
            timer += Time.deltaTime;

            // If player clicked sticker, no need to wait anymore 
            if (collected)
            {
                break;
            }
        }

        Collect();
    }

    /// <summary>
    /// Collects the sticker, triggering movement to sticker book at any point. Called from button
    /// </summary>
    public void Clicked()
    {
        Collect();

        if (AudioManager.Instance != null)
        {
            AudioManager.Instance.PlayButtonSound();
        }
    }

    private void Collect()
    {
        if (collected)
        {
            return;
        }

        collected = true;
        MoveToStickerBook();
    }

    private void MoveToStickerBook()
    {
        StopAllCoroutines();
        rectTransform.sizeDelta = originalSizeDelta;
        animator.enabled = true;
        animator.SetTrigger(ANIMATOR_TO_STICKER_BOOK_TRIGGER_KEY);
        Retire();
        // StartCoroutine(MoveSequenceToStickerBook(transform.position, StickerNotificationManager.Instance.notificationScreenSpaceFlyGoal.position));
    }

    /// <summary>
    /// Flies to stickerbook
    /// </summary>
    /// <param name="startPos"></param>
    /// <param name="goalPos"></param>
    /// <returns></returns>
    IEnumerator MoveSequenceToStickerBook(Vector3 startPos, Vector3 goalPos)
    {
        float timer = 0f;
        while (timer < flyTime)
        {
            yield return new WaitForEndOfFrame();
            timer += Time.deltaTime;

            transform.position = Vector3.Lerp(startPos, goalPos, timer / flyTime);
        }

        Retire();
    }

    private void Retire()
    {
        Destroy(gameObject, 3f);
    }

    private void OnDestroy()
    {
        StickerNotificationManager.Instance.RetireNotification(this);
        StickerNotificationManager.Instance.notificationCount--;
    }
}
