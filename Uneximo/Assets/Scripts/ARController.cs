﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
using System;

/// <summary>
/// ARController monitors Google ARCore's tracking status, creates and handles anchors and provides information
/// about the current anchor status. In ARCore project, the main camera is controlled by ARCore and ARCore's tracking
/// affects how the surrounding space is perceived and objects are placed relative to the camera. If ARCore can't track
/// any surfaces or feature points, anchor cannot be placed or tracked. In these cases objects tend to float away from the
/// view unless their positions are continuously updated to be at certain position in front of the camera. In this non-anchored
/// state, camera reacts to the tilt rotation but can't evaluate distances.
/// </summary>
public class ARController : MonoBehaviour
{
    /// <summary>
    /// A singleton instance of ARController.
    /// </summary>
    public static ARController Instance =null;
    ///<summary>
    /// Enum list of ARController state machine
    ///</summary>
    public enum State
    {
        /// <summary>
        ///  Inintial state
        /// </summary>
        idle,
        /// <summary>
        /// Searching feature points without search visualization.
        /// </summary>
        SearchingInBackground,
        /// <summary>
        /// Searching feature points (w/ visualization)
        /// </summary>
        SearchingFeaturePoint,
        /// <summary>
        /// Anchor point has been found and the active world is anchored to it.
        /// </summary>
        Anchored
    }
    /// <summary>
    /// Enum list of different anchor states.
    /// </summary>
    public enum AnchorState
    {
        /// <summary>
        /// Anchor is set to a ARCore feature point trackable.
        /// </summary>
        FeaturePoint,
        /// <summary>
        /// Anchor is placed to any detected trackable.
        /// </summary>
        Temporary,
        /// <summary>
        /// Anchor is placed to any tracked trackable.
        /// </summary>
        Any,
        /// <summary>
        /// No anchor
        /// </summary>
        UnAnchored
    }

    ///<summary>
    /// State machine state.
    ///</summary>
    State currentState = State.idle;
    /// <summary>
    /// Current anchor status. If anchor is not placed to a feature point trackable, ARController tries to find a better target for an anchor.
    /// </summary>
    AnchorState anchorState = AnchorState.UnAnchored;

    ///<summary>
    /// Active anchor.
    ///</summary>
    private Anchor anchor = null;

    /// <summary>
    /// A list to hold all planes ARCore is tracking in the current frame. This object is used across
    /// the application to avoid per-frame allocations.
    /// </summary>
    private List<Trackable> trackables = new List<Trackable>();

    /// <summary>
    /// True if the app is in the process of quitting due to an ARCore connection error, otherwise false.
    /// </summary>
    private bool m_IsQuitting = false;

    ///<summary>
    /// Trackable where the anchor is placed.
    ///</summary>
    private Trackable anchorTrackable = null;

    /// <summary>
    /// Float value in seconds how long ARController tries to fix lost feature point before visual search indicators show up.
    /// </summary>
    private const float searchTime = 2f;


    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    /// <summary>
    /// A bool delegate.
    /// </summary>
    /// <param name="anchored">New anchored status</param>
    public delegate void ARStateHandler(bool anchored);
    /// <summary>
    /// An event for sending information about ARController's anchor status changes. 
    /// </summary>
    public event ARStateHandler AnchoredStatusChange;
    /// <summary>
    /// ARController calls this function when anchor status is changed. If status changes from AnchorState.UnAnchored to any
    /// other status and vice versa, this function raises event AnchoredStatusChange.
    /// </summary>
    /// <param name="newState">The new AnchorState state</param>
    protected virtual void OnAnchoredStatusChange(AnchorState newState)
    {
        if (newState == AnchorState.UnAnchored)
        {
            if (anchorState != AnchorState.UnAnchored)
            {
                AnchoredStatusChange(false);
            }
        }
        else
        {
            if (anchorState == AnchorState.UnAnchored)
            {
                AnchoredStatusChange(true);
            }
        }
        anchorState = newState;
    }


    /// <summary>
    /// The Unity Update() method.
    /// </summary>
    void Update()
    {
        if (currentState==State.idle)
        {
            return;
        }
        UpdateApplicationLifecycle();


        //Check if there are tracked features.
        Session.GetTrackables<Trackable>(trackables);
        // TrackableHit and filter for hit testing
        TrackableHit hit;
        //TrackableHitFlags raycastFilter = TrackableHitFlags.FeaturePointWithSurfaceNormal;
        TrackableHitFlags featureFilter = TrackableHitFlags.FeaturePointWithSurfaceNormal;
        TrackableHitFlags defaultFilter = TrackableHitFlags.Default;

        switch (currentState)
        {
            case (State.SearchingInBackground):
                if (!anchor)
                {
                    if (Frame.Raycast(Screen.width / 2, Screen.height / 2, featureFilter, out hit))
                    {
                        PlaceAnchor(hit);
                        OnAnchoredStatusChange(AnchorState.FeaturePoint);
                    }
                }
                else
                {
                    if (!StillTracking())
                    {
                        RemoveAnchor();
                        OnAnchoredStatusChange(AnchorState.UnAnchored);
                    }
                }
                break;
            case (State.SearchingFeaturePoint):
                if (!anchor)
                {
                    if (Frame.Raycast(Screen.width / 2, Screen.height / 2, featureFilter, out hit))
                    {
                        PlaceAnchor(hit);
                    }
                }
                if (anchor)
                {
                    currentState = State.Anchored;
                    OnAnchoredStatusChange(AnchorState.FeaturePoint);
                }
                else
                {
                    if (Frame.Raycast(Screen.width / 2, Screen.height / 2, defaultFilter, out hit))
                    {
                        PlaceAnchor(hit);
                    }
                }
                if (anchor)
                {
                    currentState = State.Anchored;
                    OnAnchoredStatusChange(AnchorState.Temporary);
                }
                else if (trackables.Count > 0)
                {
                    if (PlaceAnchorToAnyTrackable())
                    {
                        currentState = State.Anchored;
                        OnAnchoredStatusChange(AnchorState.Any);
                    }
                }
                if (!anchor)
                {
                    currentState = State.Anchored;
                    OnAnchoredStatusChange(AnchorState.UnAnchored);
                }
                break;
            case (State.Anchored):
                if (anchorState != AnchorState.UnAnchored)
                {
                    if (!StillTracking())
                    {
                        RemoveAnchor();
                        OnAnchoredStatusChange(AnchorState.UnAnchored);
                        break;
                    }
                }
                if (anchorState != AnchorState.FeaturePoint)
                {
                    if (Frame.Raycast(Screen.width / 2, Screen.height / 2, featureFilter, out hit))
                    {
                        PlaceAnchor(hit);
                        OnAnchoredStatusChange(AnchorState.FeaturePoint);
                        break;
                    }
                    else if (anchorState == AnchorState.UnAnchored || anchorState == AnchorState.Any)
                    {
                        if (Frame.Raycast(Screen.width / 2, Screen.height / 2, defaultFilter, out hit))
                        {
                            PlaceAnchor(hit);
                            OnAnchoredStatusChange(AnchorState.Temporary);
                            break;
                        }
                    }
                    else if (anchorState == AnchorState.UnAnchored)
                    {
                        if (trackables.Count > 0)
                        {
                            if (PlaceAnchorToAnyTrackable())
                            {
                                OnAnchoredStatusChange(AnchorState.Any);
                            }
                        }
                    }
                }
                break;
        }
        
    }

    /// <summary>
    /// This is called from gamecontroller at the beginning of the session to activate AR tracking.
    /// </summary>
    public void ActivateARController()
    {
        currentState = State.SearchingInBackground;
    }
    /// <summary>
    /// This function can be called to delete the current anchor and start searching a target for a new one.
    /// </summary>
    public void ResetToSearch()
    {
        RemoveAnchor();
        GoogleARCoreInternal.LifecycleManager.Instance.ResetSession();
        currentState = State.SearchingFeaturePoint;
    }

    /// <summary>
    /// Destroys anchor and sets anchor's trackable to null.
    /// </summary>
    private void RemoveAnchor()
    {
        if (anchor)
        {
            DestroyImmediate(anchor);
        }
        anchorTrackable = null;
        OnAnchoredStatusChange(AnchorState.UnAnchored);
    }

    /// <summary>
    /// Update calls this function when anchor is set to check that it is still tracked.
    /// </summary>
    /// <returns></returns>
    private bool StillTracking()
    {
        bool tracking = false;
        for (int i = 0; i < trackables.Count; i++)
        {
            if (trackables[i].Equals(anchorTrackable))
            {
                if (trackables[i].TrackingState == TrackingState.Tracking)
                {
                    tracking = true;
                }
                break;
            }
        }

        return tracking;
    }

    /// <summary>
    /// Creates anchor and saves anchor's Trackable object to parameter anchorTrackable.
    /// </summary>
    /// <param name="hit"> Raycasted Trackable hit</param>
    private void PlaceAnchor(TrackableHit hit)
    {
        if (anchor)
        {
            DestroyImmediate(anchor);
        }
        anchor = hit.Trackable.CreateAnchor(hit.Pose);
        anchorTrackable = hit.Trackable;
    }
    
    private bool PlaceAnchorToAnyTrackable()
    {
        bool tracked = false;
        for (int i = trackables.Count -1; i>=0; i--)
        {
            if (trackables[i].TrackingState == TrackingState.Tracking)
            {
                anchorTrackable = trackables[i];
                tracked = true;
                break;
            }
        }
        if (anchor)
        {
            DestroyImmediate(anchor);
        }
        if (tracked)
        {
            Pose pose = new Pose();
            Vector3 cameraForward = Camera.main.transform.forward;
            Vector3 cameraBearing = new Vector3(cameraForward.x, 0f, cameraForward.z).normalized;
            pose.position = Camera.main.transform.position + cameraForward;
            pose.rotation = Quaternion.LookRotation(cameraBearing, Vector3.up);
            anchor = anchorTrackable.CreateAnchor(pose);
            return true;
        }
        else
        {
            return false;
        }

    }
    /// <summary>
    /// Check and update the application lifecycle.
    /// </summary>
    private void UpdateApplicationLifecycle()
    {
        // Exit the app when the 'back' button is pressed.
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        // Only allow the screen to sleep when not tracking.
        if (Session.Status != SessionStatus.Tracking)
        {
            const int lostTrackingSleepTimeout = 15;
            Screen.sleepTimeout = lostTrackingSleepTimeout;
        }
        else
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        if (m_IsQuitting)
        {
            return;
        }

        // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
        if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
        {
            ShowAndroidToastMessage("Camera permission is needed to run this application.");
            m_IsQuitting = true;
            Invoke("DoQuit", 0.5f);
        }
        else if (Session.Status.IsError())
        {
            ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
            m_IsQuitting = true;
            Invoke("DoQuit", 0.5f);
        }
    }

    /// <summary>
    /// Actually quit the application.
    /// </summary>
    private void DoQuit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Show an Android toast message.
    /// </summary>
    /// <param name="message">Message string to show in the toast.</param>
    private void ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                    message, 0);
                toastObject.Call("show");
            }));
        }
    }
}
