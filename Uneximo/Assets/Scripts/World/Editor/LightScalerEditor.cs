﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Custom editor that adds a button to the inspector. 
/// The button finds scalable components in the hierarchy. 
/// </summary>
[CustomEditor(typeof(LightScaler))]
public class LightScalerEditor : Editor
{
    int found = -1;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Find Lights"))
        {
            LightScaler scaler = (LightScaler) target;
            Undo.RecordObject(target, "Found Lights for LightScaler");
            PrefabUtility.RecordPrefabInstancePropertyModifications(target);
            found = scaler.FindLights();
        }

        if (found == 0)
        {
            EditorGUILayout.HelpBox("Unable to find any Lights in children of this object's transform.", MessageType.Warning, true);
        }
        else if (found > 0)
        {
            EditorGUILayout.HelpBox("Found " + found.ToString() + " Lights in children of this object's transform.", MessageType.None, true);
        }
    }
}
