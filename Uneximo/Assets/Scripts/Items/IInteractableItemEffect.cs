﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface for an InteractableItemEffect. 
/// Any monobehaviour that implements this and is attached to the same gameobject as an InteractableItem
/// will receive the calls for the started and stopped functions. 
/// </summary>
public interface IInteractableItemEffect
{
    void InteractingStarted();
    void InteractingStopped();
}
