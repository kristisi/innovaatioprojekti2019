﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Custom editor that adds a button to the inspector. 
/// The button finds scalable components in the hierarchy. 
/// </summary>
[CustomEditor(typeof(AudioSourceScaler))]
public class AudioSourceScalerEditor : Editor
{
    int found = -1;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Find AudioSources"))
        {
            AudioSourceScaler scaler = (AudioSourceScaler) target;
            Undo.RecordObject(target, "Found AudioSources for AudioSourceScaler");
            PrefabUtility.RecordPrefabInstancePropertyModifications(target);
            found = scaler.FindAudioSources();
        }

        if (found == 0)
        {
            EditorGUILayout.HelpBox("Unable to find any AudioSources in children of this object's transform.", MessageType.Warning, true);
        }
        else if (found > 0)
        {
            EditorGUILayout.HelpBox("Found " + found.ToString() + " AudioSources in children of this object's transform.", MessageType.None, true);
        }
    }
}
