﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Functionality for Options button. Adds ToggleOptionsPanel event to the button.
/// </summary>
public class OptionsButton : MonoBehaviour {
   
    void Start () {
        // Adds ToggleOptionsPanel event to the button.
        GetComponent<Button>().onClick.AddListener(UIController.Instance.OnToggleSettings);
    }
	
}
