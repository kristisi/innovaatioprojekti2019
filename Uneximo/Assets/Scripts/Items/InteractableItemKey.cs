﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Defines a unique Interactable item key. Used by the key and lock system. 
/// InteractableItemKeyGiver gives the player keys 
/// and InteractableItemLock can lock an InteractableItem until player has a specific key. 
/// </summary>
[CreateAssetMenu(fileName = "InteractableItemKey", menuName = "Interactable Item Key")]
public class InteractableItemKey : ScriptableObject
{
    /// <summary>
    /// Icon for the key. Used in UI. 
    /// </summary>
    public Sprite icon;
}
