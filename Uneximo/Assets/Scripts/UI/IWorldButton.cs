﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface for WorldButton functionality. 
/// </summary>
public interface IWorldButton
{
    /// <summary>
    /// Loads given world.
    /// </summary>
    /// <param name="islandToLoad">World to load.</param>
    void LoadWorld(WorldManager.WorldID islandToLoad);
}
