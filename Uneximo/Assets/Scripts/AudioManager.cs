﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Manages audio sources and playing clips that are not attached to world objects.
/// </summary>
public class AudioManager : MonoBehaviour
{
    /// <summary>
    /// A singleton instance of the class.
    /// </summary>
    public static AudioManager Instance = null;

    public AudioSource genericInteractAudioSource;
    public AudioSource genericUIAudioSource;
    public AudioSource musicAudioSource;
    public AudioSource buttonAudioSource;
    public AudioSource worldChangeAudioSource;
    public AudioSource stickerGetAudioSource;
    public AudioSource stickerPickAudioSource;
    public AudioSource stickerPlaceAudioSource;

    public bool soundsEnabled = true;
    public float fadeInMusicTime = 1f;
    public float fadeOutMusicTime = 1f;

    /// <summary>
    /// Music audioClips linked to WorldIDs. Set in editor. 
    /// </summary>
    public List<WorldMusic> worldMusics;

    private AudioClip fadeInMusicClip = null;
    private static WaitForEndOfFrame frame = new WaitForEndOfFrame();

    [System.Serializable]
    public class WorldMusic
    {
        public WorldManager.WorldID world;
        public AudioClip music;
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        // Change music whenever world changes
        GameController.Instance.WorldChangeStarted += ChangeWorldMusic;
        GameController.Instance.WorldDeactivated += PlayWorldChangeSound;
        GameController.Instance.StickerIsTouched += PlayStickerPickSound;
        GameController.Instance.StickerIsPlaced += PlayStickerPlaceSound;
    }

    public bool IsSoundEnabled()
    {
        return soundsEnabled;
    }

    /// <summary>
    /// Play generic interact sound
    /// </summary>
    public void PlayGenericInteractSound()
    {
        if (soundsEnabled)
        {
            if (genericInteractAudioSource != null && !genericInteractAudioSource.isPlaying)
            {
                genericInteractAudioSource.Play();
            }
        }
    }

    /// <summary>
    /// Plays generic UI sound
    /// </summary>
    public void PlayGenericUISound()
    {
        if (soundsEnabled)
        {
            if (genericUIAudioSource != null && !genericUIAudioSource.isPlaying)
            {
                genericUIAudioSource.Play();
            }
        }
    }

    /// <summary>
    /// Plays button sound
    /// </summary>
    public void PlayButtonSound()
    {
        if (soundsEnabled)
        {
            if (buttonAudioSource != null && !buttonAudioSource.isPlaying)
            {
                buttonAudioSource.Play();
            }
        }
    }

    /// <summary>
    /// Plays world change sound
    /// </summary>
    public void PlayWorldChangeSound()
    {
        if (soundsEnabled)
        {
            if (worldChangeAudioSource != null && !worldChangeAudioSource.isPlaying)
            {
                worldChangeAudioSource.Play();
            }
        }
    }

    /// <summary>
    /// Plays sticker "get" sound
    /// </summary>
    public void PlayStickerGetSound()
    {
        if (soundsEnabled)
        {
            if (stickerGetAudioSource != null && !stickerGetAudioSource.isPlaying)
            {
                stickerGetAudioSource.Play();
            }
        }
    }

    /// <summary>
    /// Plays sticker pick up sound (in sticker book)
    /// </summary>
    public void PlayStickerPickSound()
    {
        if (soundsEnabled)
        {
            if (stickerPickAudioSource != null && !stickerPickAudioSource.isPlaying)
            {
                stickerPickAudioSource.Play();
            }
        }
    }

    /// <summary>
    /// Plays sticker place sound (in sticker book)
    /// </summary>
    public void PlayStickerPlaceSound()
    {
        if (soundsEnabled)
        {
            if (stickerPlaceAudioSource != null && !stickerPlaceAudioSource.isPlaying)
            {
                stickerPlaceAudioSource.Play();
            }
        }
    }

    /// <summary>
    /// Toggles mute all sounds. 
    /// </summary>
    /// <param name="mute">If true will mute all sounds. If false will unmute all sounds.</param>
    public void ToggleMute(bool mute)
    {
        soundsEnabled = mute;
        if (!soundsEnabled)
        {
            if (musicAudioSource != null && musicAudioSource.isPlaying)
            {
                musicAudioSource.Pause();
            }
        }
        else
        {
            if (musicAudioSource != null && !musicAudioSource.isPlaying)
            {
                musicAudioSource.UnPause();
            }
        }
    }

    /// <summary>
    /// Changes current music to music set in worldMusics for the given WorldID
    /// </summary>
    /// <param name="worldID">WorldID used to find the correct music clip set in worldMusics</param>
    public void ChangeWorldMusic(WorldManager.WorldID worldID)
    {
        if (worldMusics != null && worldMusics.Count > 0)
        {
            foreach (WorldMusic worldMusic in worldMusics)
            {
                if (worldMusic.world == worldID)
                {
                    if (soundsEnabled)
                    {
                        // If not muted start fade out transition to other music clip
                        PlayMusic(worldMusic.music);
                    }
                    else
                    {
                        // If muted change clip directly without fade and without playing audioSource
                        musicAudioSource.clip = worldMusic.music;
                    }
                    return;
                }
            }
        }

        Debug.LogWarning("No music set for world " + Enum.GetName(typeof(WorldManager.WorldID), worldID) + " in AudioManager.");
    }
    
    /// <summary>
    /// Plays music. Fades out previous music if any then fades in music given  in parameter
    /// </summary>
    /// <param name="music">Music to play</param>
    public void PlayMusic(AudioClip music)
    {
        if (musicAudioSource.isPlaying)
        {
            if (musicAudioSource.clip != music)
            {
                fadeInMusicClip = music;
                StartCoroutine(FadeOutSound(musicAudioSource, fadeOutMusicTime, FadeInMusic));
            }
        }
        else
        {
            fadeInMusicClip = music;
            FadeInMusic();
        }
    }

    /// <summary>
    /// Fades out any music that is playing
    /// </summary>
    public void FadeOutMusic()
    {
        if (musicAudioSource.isPlaying)
        {
            StartCoroutine(FadeOutSound(musicAudioSource, fadeOutMusicTime));
        }
    }

    /// <summary>
    /// Fades in music set in fadeInMusicClip
    /// </summary>
    private void FadeInMusic()
    {
        if (fadeInMusicClip != null)
        {
            musicAudioSource.Stop();
            musicAudioSource.clip = fadeInMusicClip;
            StartCoroutine(FadeInSound(musicAudioSource, fadeInMusicTime));
            fadeInMusicClip = null;
        }
    }

    /// <summary>
    /// Fades out audioSource over fadeTime by reducing volume every frame. When finished, invokes onFinish if it is not null. 
    /// </summary>
    /// <param name="audioSource">AudioSource to fade</param>
    /// <param name="fadeTime">Time that the fade takes to finish</param>
    /// <param name="onFinish">Action to invoke when finished</param>
    public static IEnumerator FadeOutSound(AudioSource audioSource, float fadeTime, UnityAction onFinish = null)
    {
        float startVolume = audioSource.volume;
        float adjustedVolume = startVolume;
        float adjustStep = startVolume * Time.deltaTime / fadeTime;

        while (adjustedVolume > 0)
        {
            adjustedVolume -= adjustStep;
            audioSource.volume = adjustedVolume;
            yield return frame;
        }

        audioSource.Stop();
        audioSource.volume = startVolume;

        if (onFinish != null)
        {
            onFinish.Invoke();
        }
    }

    /// <summary>
    /// Fades in audioSource over fadeTime by setting volume to 0 and increasing volume every frame until original volume is reached. When finished, invokes onFinish if it is not null. 
    /// </summary>
    /// <param name="audioSource">AudioSource to fade. AudioSource volume should be set to target level.</param>
    /// <param name="fadeTime">Time that the fade takes to finish</param>
    /// <param name="onFinish">Action to invoke when finished</param>
    public static IEnumerator FadeInSound(AudioSource audioSource, float fadeTime, UnityAction onFinish = null)
    {
        float goalVolume = audioSource.volume;
        float adjustedVolume = goalVolume;
        float adjustStep = goalVolume * Time.deltaTime / fadeTime;

        audioSource.volume = 0f;
        audioSource.Play();

        while (adjustedVolume < goalVolume)
        {
            adjustedVolume += adjustStep;
            audioSource.volume = adjustedVolume;
            yield return frame;
        }

        audioSource.volume = goalVolume;

        if (onFinish != null)
        {
            onFinish.Invoke();
        }
    }
}
