﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Implements IComponentScaler. This script is added to world's root gameobject.
/// Called from WorldNavigator, it scales light sources of its child objects.
/// </summary>
public class LightScaler : MonoBehaviour, IComponentScaler
{
    /// <summary>
    /// List of lights of this world gameobject.
    /// </summary>
    public List<Light> lights;

    private Dictionary<Light, float> originalRanges;

    private void Awake()
    {
        originalRanges = new Dictionary<Light, float>();
        foreach(Light light in lights)
        {
            originalRanges.Add(light, light.range);
        }
    }
    /// <summary>
    /// Scales light childobjects.
    /// </summary>
    /// <param name="scale"></param>
    public void Scale(float scale)
    {
        foreach(Light light in lights)
        {
            light.range = originalRanges[light] * scale;
        }
    }
    /// <summary>
    /// Finds 
    /// </summary>
    /// <returns>Returns the count of lights in this world gameobject as child gameobjects.</returns>
    public int FindLights()
    {
        if (lights == null)
        {
            lights = new List<Light>();
        }
        lights.Clear();

        AddLightsInChildren(transform, lights);
        return lights.Count;
    }

    private void AddLightsInChildren(Transform parent, List<Light> list)
    {
        foreach (Transform child in parent)
        {
            if (child.GetComponent<Light>() != null)
            {
                list.AddRange(child.GetComponents<Light>());
            }
            AddLightsInChildren(child, list);
        }
    }
}
