﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Handles showing and hiding an item counter UI element. 
/// Shows how many items have been collected. 
/// </summary>
public class ItemCounterUI : MonoBehaviour
{
    /// <summary>
    /// Parent GameObject containing all the contents of this UI element. Activated and deactivated to show and hide. 
    /// </summary>
    public GameObject contents;
    /// <summary>
    /// Text where counter value is displayed. 
    /// </summary>
    public TextMeshProUGUI counterText;
    /// <summary>
    /// Time how long the UI element will be showed. 
    /// </summary>
    public float showTime = 1.5f;
    /// <summary>
    /// Time how long the show and hide transitions will take. 
    /// </summary>
    public float scaleTransitionTime = 0.1f;

    private RectTransform rectTransform;
    private Vector2 originalSizeDelta;

    private static WaitForEndOfFrame frame = new WaitForEndOfFrame();

    /// <summary>
    /// Initializes this object and sets the contents to show the given count. 
    /// Begins a timer to destroy self. Timer time is showTime + scaleTransitionTime. 
    /// </summary>
    /// <param name="currentCount">Current count</param>
    /// <param name="maxCount">Maximum count</param>
    public void Init(int currentCount, int maxCount)
    {
        rectTransform = GetComponent<RectTransform>();
        originalSizeDelta = rectTransform.sizeDelta;
        counterText.SetText(currentCount.ToString() + "/" + maxCount.ToString());

        StartCoroutine(WaitForRetire());
    }

    /// <summary>
    /// Hides the contents using a transition animation
    /// </summary>
    public void HideWithTransition()
    {
        StartCoroutine(ScaleDownSequence());
    }

    /// <summary>
    /// Shows the contents using a transition animation
    /// </summary>
    public void ShowWithTransition()
    {
        contents.SetActive(true);
        StartCoroutine(ScaleUpSequence());
    }

    /// <summary>
    /// Hides the contents without a transition
    /// </summary>
    public void Hide()
    {
        contents.SetActive(false);
        // StartCoroutine(ScaleDownSequence());
    }

    /// <summary>
    /// Shows the contents without a transition
    /// </summary>
    public void Show()
    {
        contents.SetActive(true);
        // StartCoroutine(ScaleUpSequence());
    }

    IEnumerator WaitForRetire()
    {
        yield return new WaitForSeconds(showTime);
        HideWithTransition();
        yield return new WaitForSeconds(scaleTransitionTime);
        Retire();
    }

    IEnumerator ScaleUpSequence()
    {
        rectTransform.sizeDelta = Vector2.zero;
        float timer = 0f;
        while (timer < scaleTransitionTime)
        {
            yield return frame;
            timer += Time.deltaTime;

            float lerpValue = timer / scaleTransitionTime;

            rectTransform.sizeDelta = Vector2.Lerp(Vector2.zero, originalSizeDelta, lerpValue);
        }

        // Set size to exact goal
        rectTransform.sizeDelta = originalSizeDelta;
    }

    IEnumerator ScaleDownSequence()
    {
        float timer = 0f;
        while (timer < scaleTransitionTime)
        {
            yield return frame;
            timer += Time.deltaTime;

            float lerpValue = timer / scaleTransitionTime;

            rectTransform.sizeDelta = Vector2.Lerp(originalSizeDelta, Vector2.zero, lerpValue);
        }

        // Set size to exact goal
        rectTransform.sizeDelta = Vector2.zero;
        
        contents.SetActive(false);
    }

    private void Retire()
    {
        InteractableItemCounterManager.Instance.RetireNotification(this);
        Destroy(gameObject);
    }
}
