﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles player interacting with Interactable Items by tapping the item
/// </summary>
public class InteractByTap : MonoBehaviour
{
    private void Start()
    {
        if (PlayerInput.Instance == null)
        {
            Debug.LogError("Add PlayerInput script to the scene");
        }

        PlayerInput.Instance.TouchBegan += Interact;
    }

    void Interact(Touch touch)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(touch.position);
        LayerMask mask = LayerMask.GetMask("InteractableItems", "MainGeoLayer");

        if (Physics.Raycast(ray, out hit, 100f, mask))
        {
            InteractableItem item = hit.collider.GetComponentInParent<InteractableItem>();

            if (item != null)
            {
                item.Interact();
            }
        }
    }
}
