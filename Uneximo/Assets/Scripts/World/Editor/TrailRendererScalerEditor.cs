﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Custom editor that adds a button to the inspector. 
/// The button finds scalable components in the hierarchy. 
/// </summary>
[CustomEditor(typeof(TrailRendererScaler))]
public class TrailRendererScalerEditor : Editor
{
    int found = -1;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Find Trail Renderers"))
        {
            TrailRendererScaler scaler = (TrailRendererScaler) target;
            Undo.RecordObject(target, "Found Trail Renderers for TrailRendererScaler");
            PrefabUtility.RecordPrefabInstancePropertyModifications(target);
            found = scaler.FindTrailRenderers();
        }

        if (found == 0)
        {
            EditorGUILayout.HelpBox("Unable to find any TrailRenderers in children of this object's transform.", MessageType.Warning, true);
        }
        else if (found > 0)
        {
            EditorGUILayout.HelpBox("Found " + found.ToString() + " TrailRenderers in children of this object's transform.", MessageType.None, true);
        }
    }
}
