﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles player interacting with Interactable Items by pointing reticle at the item
/// </summary>
public class InteractByLooking : MonoBehaviour
{
    /// <summary>
    /// How long player needs to hold target item in the reticle before interaction
    /// </summary>
    public float interactTime = 2f;

    private float interactTimer = 0f;
    private InteractableItem interactingItem;

    private InteractLookReticle reticle;

    private bool interactionEnabled = true;

    /// <summary>
    /// Disables interaction by look reticle and hides look reticle
    /// </summary>
    public void DisableInteraction()
    {
        interactionEnabled = false;
        SetInteractingItemToNull();
        reticle.Hide();
    }

    /// <summary>
    /// Enables interaction by look reticle and shows look reticle
    /// </summary>
    public void EnableInteraction()
    {
        interactionEnabled = true;
        reticle.Show();
    }

    private void Awake()
    {
        reticle = FindObjectOfType<InteractLookReticle>();
        if (reticle == null)
        {
            Debug.LogError("Cannot find InteractLookReticle in scene. ");
        }
    }

    void Update()
    {
        if (interactionEnabled)
        {
            LookForInteractableItem();
            CheckInteractTimer();
        }
    }

    /// <summary>
    /// Raycasts into the world from screen center and looks for interactable items hit. 
    /// Starts the interaction timer if an interactable item is found. 
    /// </summary>
    void LookForInteractableItem()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f));
        LayerMask mask = LayerMask.GetMask("InteractableItems", "MainGeoLayer");

        // if we hit something
        if (Physics.Raycast(ray, out hit, 100f, mask))
        {
            InteractableItem item = hit.collider.GetComponentInParent<InteractableItem>();
            // if we hit an item
            if (item != null)
            {
                // if that item is not interactable right now
                if (!item.IsInteractable())
                {
                    SetInteractingItemToNull();
                    return;
                }

                // if the hit interactable item is different from previously hit item
                if (item != interactingItem)
                {
                    SetNewInteractingItem(item);
                    return;
                }
            }
            else // we hit something other than a interactable item
            {
                SetInteractingItemToNull();
                return;
            }
        }
        else // we didnt hit anything
        {
            SetInteractingItemToNull();
            return;
        }
    }

    void SetNewInteractingItem(InteractableItem item)
    {
        interactingItem = item;
        interactTimer = 0f;

        reticle.StartTargeting();
    }

    void SetInteractingItemToNull()
    {
        interactingItem = null;
        interactTimer = 0f;

        reticle.ResetTargeting();
    }

    void TriggerInteract()
    {
        interactingItem.Interact();
        reticle.FinishTargeting();
        SetInteractingItemToNull();
    }

    /// <summary>
    /// Checks and increments the interaction timer and triggers interaction if timer reaches interactTime. 
    /// </summary>
    void CheckInteractTimer()
    {
        if (interactingItem != null)
        {
            if (interactTimer >= interactTime)
            {
                TriggerInteract();
            }
            else
            {
                interactTimer += Time.deltaTime;
            }
        }
    }
}
