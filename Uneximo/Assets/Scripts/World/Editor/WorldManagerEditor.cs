﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// Custom editor that adds a button to the inspector. 
/// The button respawns the current world.
/// Obsolete, not needed. 
/// </summary>
[CustomEditor(typeof(WorldManager))]
public class WorldManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Place island"))
        {
            GameObject anchor = new GameObject("Anchor");
            anchor.transform.position = Vector3.zero;
            ((WorldManager)target).RespawnWorld();
        }
    }
}
