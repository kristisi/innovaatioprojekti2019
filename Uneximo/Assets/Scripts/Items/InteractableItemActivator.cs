﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interactable item effect that can activate or deactive a GameObject. 
/// </summary>
public class InteractableItemActivator : MonoBehaviour, IInteractableItemEffect
{
    /// <summary>
    /// Target GameObject that will be activated or deactivated
    /// </summary>
    [Tooltip("Target GameObject that will be activated or deactivated")]
    public GameObject target;
    
    /// <summary>
    /// Whether target should activated or deactivated
    /// </summary>
    [Tooltip("Whether target should activated or deactivated")]
    public bool activate = true;
    
    /// <summary>
    /// If true, TriggerAnimator must be called from event trigger in animation.  
    /// If false, automatically triggers at the end of interaction. 
    /// </summary>
    [Tooltip("If true, TriggerAnimator must be called from event trigger in animation. If false, automatically triggers at the end of interaction.")]
    public bool customTriggerTime = false;

    /// <summary>
    /// Activates or deactivates target GameObject based on "activate" bool
    /// </summary>
    public void ActivateObject()
    {
        if (target == null)
        {
            Debug.LogWarning("Target GameObject not set in InteractableItemActivator");
            return;
        }

        target.SetActive(activate);
    }

    public void InteractingStarted()
    {
        
    }

    /// <summary>
    /// Activates if customTriggerTime is false
    /// </summary>
    public void InteractingStopped()
    {
        if (!customTriggerTime)
        {
            ActivateObject();
        }
    }
}
