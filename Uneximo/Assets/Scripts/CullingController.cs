﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CullingController : MonoBehaviour
{
    /// <summary>
    /// The singleton instance of the class.
    /// </summary>
    public static CullingController Instance;
    /// <summary>
    /// RectTransform of Viewport of the scene. Set in inspector.
    /// </summary>
    public RectTransform viewport;

    public Transform hiddenPosition;
    public Transform stickerbookVisiblePosition;
    public Transform gameplayUIvisiblePosition;

    private void Start()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }

        UIController.Instance.HideStickerBook += ShowOnlyGameplay; //

        UIController.Instance.ShowStickerBook += ShowOnlyStickerbook; //

        UIController.Instance.StickerbookDragStarted += AddStickerbookMask; //

        GameController.Instance.GameControllerReady += ShowOnlyTitlescreen;

        GameController.Instance.TitlescreenClosed += ShowOnlyGameplay; //


        //GameController.Instance.TitlescreenClosed += HideBook;
        //UIController.Instance.ShowStickerBook += ShowBook;
        //UIController.Instance.StickerbookDragStarted += ShowBook;
        //UIController.Instance.HideStickerBook += HideBook;
    }

    public void ShowOnlyTitlescreen()
    {
        StickerBookUI.Instance.MoveStickerbook(hiddenPosition);
        GameplayUI.Instance.MoveGameplayUI(hiddenPosition);
    }
    public void ShowOnlyGameplay()
    {
        StickerBookUI.Instance.MoveStickerbook(hiddenPosition);
        GameplayUI.Instance.MoveGameplayUI(gameplayUIvisiblePosition);
    }

    public void ShowOnlyStickerbook()
    {
        StickerBookUI.Instance.MoveStickerbook(stickerbookVisiblePosition);
        GameplayUI.Instance.MoveGameplayUI(hiddenPosition);
    }


    public void AddStickerbookMask()
    {
        StickerBookUI.Instance.MoveStickerbook(stickerbookVisiblePosition);
    }
}
