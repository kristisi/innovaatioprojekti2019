﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Use LoadingScreenTutorialController to handle this class.
/// </summary>
public class LoadingScreenTutorial : MonoBehaviour
{
    /// <summary>
    /// Called when tutorial animation is completed.
    /// </summary>
    public void AnimationComplete()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Called when animation reaches scaling down point.
    /// </summary>
    public void AnimationScaledownPoint()
    {
        StickerBook.Instance.AddSticker(StickerBook.Instance.tutorialSticker);
        LoadingScreenTutorialController.Instance.EndTutorial();
    }
}
