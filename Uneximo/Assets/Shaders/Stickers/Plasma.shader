﻿Shader "Sticker/Plasma" {
	Properties{
		[PerRendererData] _MainTex("Base (RGB)", 2D) = "white" {}
		[Header(Stencil)]
		_StencilComp("Stencil Comparison", Float) = 8
		_Stencil("Stencil ID", Float) = 0
		_StencilOp("Stencil Operation", Float) = 0
		_StencilWriteMask("Stencil Write Mask", Float) = 255
		_StencilReadMask("Stencil Read Mask", Float) = 255
		[Header(Rendering)]
		_ColorMask("Color Mask", Float) = 15
		_Tint("Tint", Color) = (1, 1, 1, 1)
		_Speed("Speed", Range(1,100)) = 10
		_Scale1("Scale 1", Range(1,10)) = 2
		_Scale2("Scale 2", Range(1,10)) = 2
		_Scale3("Scale 3", Range(1,10)) = 2
		_Scale4("Scale 4", Range(1,10)) = 2
	}
		SubShader{
			Tags {"Queue" = "Transparent" "RenderType" = "Transparent"}
			Stencil
			{
				Ref[_Stencil]
				Comp[_StencilComp]
				Pass[_StencilOp]
				ReadMask[_StencilReadMask]
				WriteMask[_StencilWriteMask]
			}
			//Cull Off
			ZWrite off
			Lighting Off
			ZTest[unity_GUIZTestMode]
			ColorMask[_ColorMask]
			Blend One OneMinusSrcAlpha

			Pass {

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"

				sampler2D _MainTex;

				struct v2f {
					float4 pos : SV_POSITION;
					half2 uv : TEXCOORD0;
				};

				v2f vert(appdata_base v) {
					v2f o;
					o.pos = UnityObjectToClipPos(v.vertex);
					o.uv = v.texcoord;
					return o;
				}

				fixed4 _Tint;
				float4 _MainTex_TexelSize;
				half _Speed;
				half _Scale1;
				half _Scale2;
				half _Scale3;
				half _Scale4;

				fixed4 frag(v2f i) : COLOR
				{
					half4 col = tex2D(_MainTex, i.uv);
					col.rgb = (col.r + col.g + col.b)/ 3;
					col.rgb *= col.a*0.5;

					const float PI = 3.14159265;
					half t = _Time.x * _Speed;

					//vertical, stripes
					float c = sin(i.uv.x *_Scale1 + t);
					//horizontal, boxes
					c += sin(i.uv.y *_Scale2 + t);

					//diagonal, plasma patterns
					c += sin(_Scale3*(i.uv.x*sin(t / 2.0) + i.uv.y*cos(t / 3.0)) + t);

					float c1 = pow(i.uv.x + 0.5 * sin(t / 5.0), 2);
					float c2 = pow(i.uv.y + 0.5 * cos(t / 3.0), 2);
					c += sin(sqrt(_Scale4*(c1 + c2) + 1 + t));

					col.r += sin(c / 4.0*PI)*col.a/2;
					col.g += sin(c / 4.0*PI + 2 * PI / 4)*col.a/2;
					col.b += sin(c / 4.0*PI + 4 * PI / 4)*col.a/2;
					col *= _Tint;

					return col;
				}

				ENDCG
			}
		}
			FallBack "Diffuse"
}
