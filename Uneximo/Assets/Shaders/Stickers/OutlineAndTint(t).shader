﻿Shader "Sticker/Outline and Tint (transp)" {
	Properties{
		[PerRendererData] _MainTex("Base (RGB)", 2D) = "white" {}
		[Header(Stencil)]
		_StencilComp("Stencil Comparison", Float) = 8
		_Stencil("Stencil ID", Float) = 0
		_StencilOp("Stencil Operation", Float) = 0
		_StencilWriteMask("Stencil Write Mask", Float) = 255
		_StencilReadMask("Stencil Read Mask", Float) = 255
		[Header(Rendering)]
		_ColorMask("Color Mask", Float) = 15
		_Color("Outline Color", Color) = (1, 1, 1, 1)
		_Tint("Tint Color", Color) = (1, 1, 1, 1)
		_TintAmount("Tint Amount", Range(0, 1)) = 0.5
		_Width("Outline Width", Range(0, 10)) = 1
	}
		SubShader{
			Tags {"Queue" = "Transparent" "RenderType" = "Transparent"}
			Stencil
			{
				Ref[_Stencil]
				Comp[_StencilComp]
				Pass[_StencilOp]
				ReadMask[_StencilReadMask]
				WriteMask[_StencilWriteMask]
			}
			//Cull Off
			ZWrite off
			Lighting Off
			ZTest[unity_GUIZTestMode]
			ColorMask[_ColorMask]
			Blend One OneMinusSrcAlpha

			Pass {

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"

				sampler2D _MainTex;

				struct v2f {
					float4 pos : SV_POSITION;
					half2 uv : TEXCOORD0;
				};

				v2f vert(appdata_base v) {
					v2f o;
					o.pos = UnityObjectToClipPos(v.vertex);
					o.uv = v.texcoord;
					return o;
				}

				fixed4 _Color;
				fixed4 _Tint;
				half _Width;
				half _TintAmount;
				float4 _MainTex_TexelSize;

				fixed4 frag(v2f i) : COLOR
				{
					half4 c = tex2D(_MainTex, i.uv);
					c.rgb *= c.a;
					half4 outlineC = _Color;
					outlineC.a *= ceil(c.a);
					outlineC.rgb *= outlineC.a;

					fixed alpha_up = tex2D(_MainTex, i.uv + fixed2(0, _MainTex_TexelSize.y*_Width)).a;
					fixed alpha_down = tex2D(_MainTex, i.uv - fixed2(0, _MainTex_TexelSize.y*_Width)).a;
					fixed alpha_right = tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x*_Width, 0)).a;
					fixed alpha_left = tex2D(_MainTex, i.uv - fixed2(_MainTex_TexelSize.x*_Width, 0)).a;

					fixed alpha_upRight = tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.y*_Width, _MainTex_TexelSize.y*_Width)).a;
					fixed alpha_upLeft = tex2D(_MainTex, i.uv + fixed2(-_MainTex_TexelSize.y*_Width, _MainTex_TexelSize.y*_Width)).a;
					fixed alpha_downRight = tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.y*_Width, -_MainTex_TexelSize.y*_Width)).a;
					fixed alpha_downLeft = tex2D(_MainTex, i.uv + fixed2(-_MainTex_TexelSize.y*_Width, -_MainTex_TexelSize.y*_Width)).a;

					//return lerp(outlineC, c, ceil(alpha_up * alpha_down * alpha_right * alpha_left));
					return lerp(outlineC, (c*(1-_TintAmount) + _Tint*_TintAmount)/2.0, ceil(alpha_up * alpha_down * alpha_right * alpha_left
						* alpha_upRight * alpha_upLeft * alpha_downRight * alpha_downLeft));
				}

				ENDCG
			}
		}
			FallBack "Diffuse"
}
