﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Custom editor for InteractableItem. Adds a button to test the interaction. 
/// </summary>
[CustomEditor(typeof(InteractableItem))]
public class InteractableItemEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        InteractableItem item = (InteractableItem) target;

        if (GUILayout.Button("Test interaction"))
        {
            item.Interact();
        }
    }
}
