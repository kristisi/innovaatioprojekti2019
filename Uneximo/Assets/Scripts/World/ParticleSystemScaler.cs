﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Implements IComponentScaler. This script is added to world's root gameobject.
/// Called from WorldNavigator, it scales particle systems of its child objects.
/// </summary>
public class ParticleSystemScaler : MonoBehaviour, IComponentScaler
{
    /// <summary>
    /// List of particle systems in the world gameobject this script is attached.
    /// </summary>
    public List<ParticleSystem> particleSystems;

    private Dictionary<ParticleSystem, float> originalRatesOverDistance;
    private Dictionary<ParticleSystem, float> originalGravityModifiers;
    private Dictionary<ParticleSystem, float> originalNoiseStrength;

    private void Awake()
    {
        originalRatesOverDistance = new Dictionary<ParticleSystem, float>();
        originalGravityModifiers = new Dictionary<ParticleSystem, float>();
        originalNoiseStrength = new Dictionary<ParticleSystem, float>();

        foreach(ParticleSystem particleSystem in particleSystems)
        {
            originalRatesOverDistance.Add(particleSystem, particleSystem.emission.rateOverDistanceMultiplier);
            originalGravityModifiers.Add(particleSystem, particleSystem.main.gravityModifierMultiplier);
            originalNoiseStrength.Add(particleSystem, particleSystem.noise.strengthMultiplier);
        }
    }
    /// <summary>
    /// Called from worldNavigator, scales particle systems from particleSystems lists.
    /// </summary>
    /// <param name="scale"></param>
    public void Scale(float scale)
    {
        foreach(ParticleSystem particleSystem in particleSystems)
        {
            ParticleSystem.EmissionModule emissionModule = particleSystem.emission;
            if (scale != 0f)
            {
                emissionModule.rateOverDistanceMultiplier = originalRatesOverDistance[particleSystem] / scale;
            }
            else
            {
                emissionModule.rateOverDistanceMultiplier = 0f;
            }

            ParticleSystem.MainModule mainModule = particleSystem.main;
            mainModule.gravityModifierMultiplier = originalGravityModifiers[particleSystem] * scale;

            ParticleSystem.NoiseModule noiseModule = particleSystem.noise;
            noiseModule.strengthMultiplier = originalNoiseStrength[particleSystem] * scale;
        }
    }

    public int FindParticleSystems()
    {
        if (particleSystems == null)
        {
            particleSystems = new List<ParticleSystem>();
        }
        particleSystems.Clear();

        AddParticleSystemsInChildren(transform, particleSystems);
        return particleSystems.Count;
    }

    private void AddParticleSystemsInChildren(Transform parent, List<ParticleSystem> list)
    {
        foreach (Transform child in parent)
        {
            if (child.GetComponent<ParticleSystem>() != null)
            {
                list.AddRange(child.GetComponents<ParticleSystem>());
            }
            AddParticleSystemsInChildren(child, list);
        }
    }
}
