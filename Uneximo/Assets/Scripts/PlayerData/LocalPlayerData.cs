﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

/// <summary>
/// Implements IPlayerSaveData, handles data saving locally.
/// </summary>
public class LocalPlayerData : MonoBehaviour, IPlayerSaveData
{
    public static LocalPlayerData iPlayerSaveData;
    /// <summary>
    /// Set side [200] boolean array.
    /// </summary>
    public bool[] boolData;
    /// <summary>
    /// An array for serializing placed sticker data.
    /// </summary>
    public StickerData[] stickerData;
    /// <summary>
    /// A MiscData data object for serialization;
    /// </summary>
    public MiscData misc;
    private const string stickerPath1 = "/stickerData1.dat";
    private const string stickerPath2 = "/stickerData2.dat";
    private const string placedStickersPath1 = "/placedStickersData1.dat";
    private const string placedStickersPath2 = "/placedStickersData2.dat";
    private const string miscPath1 = "/mmm1.dat";
    private const string miscPath2 = "/mmm2.dat";


    private enum Datatype
    {
        StickerData
    }

    private void Awake()
    {
        if (iPlayerSaveData == null)
        {
            DontDestroyOnLoad(gameObject);
            iPlayerSaveData = this;
        }
        else if (iPlayerSaveData != this)
        {
            Destroy(gameObject);
        }
    } 

    private void SaveBoolData(Datatype type, bool[] dataArray)
    {
        string pathToSave = "";
        string backUpPath = "";

        switch (type)
        {
            case (Datatype.StickerData):
                pathToSave = stickerPath1;
                backUpPath = stickerPath2;
                break;
        }

        BinaryFormatter bf = new BinaryFormatter();
        using (FileStream file = File.Create(Application.persistentDataPath + pathToSave), 
            file2 = File.Create(Application.persistentDataPath + backUpPath))
        {
            PlayerBoolData data = new PlayerBoolData();
            data.boolData = dataArray;
            bf.Serialize(file, data);
            file.Close();
            bf.Serialize(file2, data);
            file2.Close();
        }
    }

    private void LoadBoolData(Datatype type)
    {
        bool loaded = false;
        string dataPath = "";
        string backupPath = "";
        switch (type)
        {
            case (Datatype.StickerData):
                dataPath = stickerPath1;
                backupPath = stickerPath2;
                break;
        }

        if (File.Exists(Application.persistentDataPath + dataPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream file = File.Open(Application.persistentDataPath + dataPath, FileMode.Open))
            {
                PlayerBoolData data = (PlayerBoolData)bf.Deserialize(file);
                for (int i = 0; i < boolData.Length; i++)
                {
                    boolData[i] = data.boolData[i];
                }
                file.Close();
                loaded = true;
            }
        }
        if (!loaded)
        {
            if (File.Exists(Application.persistentDataPath + backupPath))
            {
                BinaryFormatter bf = new BinaryFormatter();
                using (FileStream file = File.Open(Application.persistentDataPath + backupPath, FileMode.Open))
                {
                    PlayerBoolData data = (PlayerBoolData)bf.Deserialize(file);
                    for (int i = 0; i < boolData.Length; i++)
                    {
                        boolData[i] = data.boolData[i];
                    }
                    file.Close();
                    loaded = true;
                }
            }
        }
        if (!loaded)
        {
            for (int i = 0; i < boolData.Length; i++)
            {
                boolData[i] = false;
            }
            SaveBoolData(type, boolData);
        }
    }
    /// <summary>
    /// Returns boolean array of activation status of stickers.
    /// </summary>
    /// <returns>Boolean array.</returns>
    public bool[] LoadStickers()
    {
        LoadBoolData(Datatype.StickerData);
        return boolData;
    }
    /// <summary>
    /// Saves the status of the stickers locally.
    /// </summary>
    /// <param name="stickers">Boolean array.</param>
    public void SaveStickers(bool[] stickers)
    {
        SaveBoolData(Datatype.StickerData, stickers);
    }
    
    /// <summary>
    /// Load data of placed stickers in stickerbook.
    /// </summary>
    /// <returns>Data of stickers placed in stickerbook.</returns>
    public StickerData[] LoadPlacedStickers()
    {

        bool loaded = false;
        if (File.Exists(Application.persistentDataPath + placedStickersPath1))
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream file = File.Open(Application.persistentDataPath + placedStickersPath1, FileMode.Open))
            {
                PlacedStickersData data = (PlacedStickersData)bf.Deserialize(file);
                for (int i = 0; i < stickerData.Length; i++)
                {
                    stickerData[i] = data.stickerData[i];
                }
                file.Close();
                loaded = true;
            }
        }
        if (!loaded)
        {
            if (File.Exists(Application.persistentDataPath + placedStickersPath2))
            {
                BinaryFormatter bf = new BinaryFormatter();
                using (FileStream file = File.Open(Application.persistentDataPath + placedStickersPath2, FileMode.Open))
                {
                    PlacedStickersData data = (PlacedStickersData)bf.Deserialize(file);
                    for (int i = 0; i < stickerData.Length; i++)
                    {
                        stickerData[i] = data.stickerData[i];
                    }
                    file.Close();
                    loaded = true;
                }
            }
        }
        if (!loaded)
        {
            for (int i = 0; i < stickerData.Length; i++)
            {
                stickerData[i] = null;
            }
            SavePlacedStickers(stickerData);
        }
        return stickerData;
    }

    /// <summary>
    /// Save data of sticker placements.
    /// </summary>
    /// <param name="stickerData">Data of stickers placed in stickerbook.</param>
    public void SavePlacedStickers(StickerData[] data)
    {
        if (data.Length != stickerData.Length)
        {
            return;
        }
        else
        {
            stickerData = data;
        }
        BinaryFormatter bf = new BinaryFormatter();
        using (FileStream file = File.Create(Application.persistentDataPath + placedStickersPath1),
            file2 = File.Create(Application.persistentDataPath + placedStickersPath2))
        {
            PlacedStickersData newData = new PlacedStickersData();
            newData.stickerData = stickerData;
            bf.Serialize(file, newData);
            file.Close();
            bf.Serialize(file2, newData);
            file2.Close();
        }
    }
    /// <summary>
    /// Resests the save data of obtained stickers.
    /// </summary>
    public void ResetStickers()
    {
        for (int i=0; i < boolData.Length; i++)
        {
            boolData[i] = false;
        }
        SaveBoolData(Datatype.StickerData, boolData);
    }
    /// <summary>
    /// Resests the save data of placed stickers.
    /// </summary>
    public void ResetPlacedStickers()
    {
        for (int i=0; i < stickerData.Length; i++)
        {
            stickerData[i] = null;
        }
        SavePlacedStickers(stickerData);
    }
    /// <summary>
    /// Resest all saved data.
    /// </summary>
    public void ResetAllSavedData()
    {
        ResetStickers();
        ResetPlacedStickers();
        ResetMiscData();
    }
    /// <summary>
    /// Loads saved miscellaneous data.
    /// </summary>
    /// <returns></returns>
    public MiscData LoadMiscData()
    {
        bool loaded = false;
        if (File.Exists(Application.persistentDataPath + miscPath1))
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream file = File.Open(Application.persistentDataPath + miscPath1, FileMode.Open))
            {
                MiscData data = (MiscData)bf.Deserialize(file);
                misc.pointerTutorialCompleted = data.pointerTutorialCompleted;
                misc.startTutorialCompleted = data.startTutorialCompleted;
                misc.giftReceived = data.giftReceived;
                file.Close();
                loaded = true;
            }
        }
        if (!loaded)
        {
            if (File.Exists(Application.persistentDataPath + miscPath2))
            {
                BinaryFormatter bf = new BinaryFormatter();
                using (FileStream file = File.Open(Application.persistentDataPath + miscPath2, FileMode.Open))
                {
                    MiscData data = (MiscData)bf.Deserialize(file);
                    misc.startTutorialCompleted = data.startTutorialCompleted;
                    misc.pointerTutorialCompleted = data.pointerTutorialCompleted;
                    misc.giftReceived = data.giftReceived;
                    file.Close();
                    loaded = true;
                }
            }
        }
        if (!loaded)
        {
            misc.startTutorialCompleted = false;
            misc.pointerTutorialCompleted = false;
            misc.giftReceived = DateTime.Now;
        }
        return misc;
    }
    /// <summary>
    /// Saves miscellaneous game save data.
    /// </summary>
    /// <param name="current">A MiscData object.</param>
    public void SaveMiscData(MiscData current)
    {
        misc = current;
        BinaryFormatter bf = new BinaryFormatter();
        using (FileStream file = File.Create(Application.persistentDataPath + miscPath1),
            file2 = File.Create(Application.persistentDataPath + miscPath2))
        {
            bf.Serialize(file, misc);
            file.Close();
            bf.Serialize(file2, misc);
            file2.Close();
        }
    }
    /// <summary>
    /// Resests saved miscellaneous game save data.
    /// </summary>
    public void ResetMiscData()
    {
        misc.startTutorialCompleted = false;
        misc.pointerTutorialCompleted = false;
        misc.giftReceived = DateTime.Now;
        SaveMiscData(misc);
    }
}
