%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: astronaut_action_mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Astronaut_geo
    m_Weight: 1
  - m_Path: grp_asronaut_joints
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip
    m_Weight: 0
  - m_Path: grp_asronaut_joints/hip/spine
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/spine/head
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/spine/head/end_head
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/spine/shoulder_l
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/spine/shoulder_l/arm_l
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/spine/shoulder_l/arm_l/hand_l
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/spine/shoulder_l/arm_l/hand_l/palm_l
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/spine/shoulder_r
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/spine/shoulder_r/arm_r
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/spine/shoulder_r/arm_r/hand_r
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/spine/shoulder_r/arm_r/hand_r/palm_r
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/thigh_l
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/thigh_l/leg_l
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/thigh_l/leg_l/foot_l
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/thigh_l/leg_l/foot_l/end_foot_l
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/thigh_r
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/thigh_r/leg_r
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/thigh_r/leg_r/foot_r
    m_Weight: 1
  - m_Path: grp_asronaut_joints/hip/thigh_r/leg_r/foot_r/end_foot_r
    m_Weight: 1
