﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Custom editor for InteractableItem. Adds a button to test the interaction. 
/// </summary>
[CustomEditor(typeof(InteractableItemCounterIncrementor))]
public class InteractableItemCounterIncrementorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        string parameterName = InteractableItemCounterIncrementor.ANIMATOR_COUNTER_FINISHED_KEY;
        EditorGUILayout.HelpBox("When  the counter finishes, the animator set in an attached InteractableItem will receive a Trigger named \"" + parameterName + "\"", MessageType.Info, true);
    }
}
