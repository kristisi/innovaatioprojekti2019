﻿Shader "Sticker/FlashyPulse" {
	Properties{
		[PerRendererData] _MainTex("Base (RGB)", 2D) = "white" {}
		[Header(Stencil)]
		_StencilComp("Stencil Comparison", Float) = 8
		_Stencil("Stencil ID", Float) = 0
		_StencilOp("Stencil Operation", Float) = 0
		_StencilWriteMask("Stencil Write Mask", Float) = 255
		_StencilReadMask("Stencil Read Mask", Float) = 255
		[Header(Rendering)]
		_ColorMask("Color Mask", Float) = 15
		_Color("Tint Color", Color) = (0.95, 0.93, 0.49, 1)
		_TintAmount("Tint Amount", Range(0, 1)) = 0.5
		_Speed("Flash Speed", Range(1, 100)) = 20
		_Width("Flash Width", Range(0.001, 0.05)) = 0.05
		_Phaze("Phaze Length", Range (1, 20)) = 4
		_PulseSpeed("Pulse Speed", Range(1, 100)) = 20
		_PulseAmount("Pulse Amount", Range(0.1, 1)) = 0.5
	}
		SubShader{
			Tags {"Queue" = "Transparent" "RenderType" = "Transparent"}
			Stencil
			{
				Ref[_Stencil]
				Comp[_StencilComp]
				Pass[_StencilOp]
				ReadMask[_StencilReadMask]
				WriteMask[_StencilWriteMask]
			}
			//Cull Off
			ZWrite off
			Lighting Off
			ZTest[unity_GUIZTestMode]
			ColorMask[_ColorMask]
			Blend One OneMinusSrcAlpha

			Pass {

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"

				sampler2D _MainTex;

				struct v2f {
					float4 pos : SV_POSITION;
					half2 uv : TEXCOORD0;
				};

				v2f vert(appdata_base v) {
					v2f o;
					o.pos = UnityObjectToClipPos(v.vertex);
					o.uv = v.texcoord;
					return o;
				}

				fixed4 _Color;
				half _TintAmount;
				half _Speed;
				half _Width;
				half _Phaze;
				float4 _MainTex_TexelSize;
				half _PulseSpeed;
				half _PulseAmount;

				fixed4 frag(v2f i) : COLOR
				{
					half4 c = tex2D(_MainTex, i.uv);
					c.rgb = c.rgb*(1 - _TintAmount) + _Color * _TintAmount;

					half flashPosition_X = (_Time.x*_Speed) % _Phaze - i.uv.x;
					half flashPosition_Y = (_Time.x*_Speed) % _Phaze - (1- i.uv.y);
					half flashPosition = abs((flashPosition_X+ flashPosition_Y)/2);
					c.rgb += flashPosition != 0 ? _Width/flashPosition : 0;
					fixed p = (_PulseAmount + sin(_Time.x*_PulseSpeed)*_PulseAmount) / 2.0;
					c.rgb -= p * c.rgb;
					c.rgb *= c.a;

					return c;
				}

				ENDCG
			}
		}
			FallBack "Diffuse"
}
