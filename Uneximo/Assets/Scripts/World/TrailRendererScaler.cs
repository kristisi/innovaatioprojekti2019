﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Implements IComponentScaler. This script is added to world's root gameobject.
/// Called from WorldNavigator, it scales trail renderer components of its child objects.
/// </summary>
public class TrailRendererScaler : MonoBehaviour, IComponentScaler
{
    /// <summary>
    /// List of trail renderer components of this world's child objects.
    /// </summary>
    public List<TrailRenderer> trailRenderers;

    private Dictionary<TrailRenderer, float> originalMinVertexDistances;
    private Dictionary<TrailRenderer, float> originalWidths;

    private void Awake()
    {
        originalMinVertexDistances = new Dictionary<TrailRenderer, float>();
        originalWidths = new Dictionary<TrailRenderer, float>();
        foreach(TrailRenderer trail in trailRenderers)
        {
            originalMinVertexDistances.Add(trail, trail.minVertexDistance);
            originalWidths.Add(trail, trail.widthMultiplier);
        }
    }
    /// <summary>
    /// Called from WorldNavigator, scales trail renderers in trailRenderers list.
    /// </summary>
    /// <param name="scale"></param>
    public void Scale(float scale)
    {
        foreach(TrailRenderer trail in trailRenderers)
        {
            trail.minVertexDistance = originalMinVertexDistances[trail] * scale;
            trail.widthMultiplier = originalWidths[trail] * scale;
        }
    }
    
    /// <summary>
    /// Initializes trailRenderers list.
    /// </summary>
    /// <returns>Returns the count of trail renderers in trailRenderes list.</returns>
    public int FindTrailRenderers()
    {
        if (trailRenderers == null)
        {
            trailRenderers = new List<TrailRenderer>();
        }
        trailRenderers.Clear();

        AddTrailRenderersInChildren(transform, trailRenderers);
        return trailRenderers.Count;
    }

    private void AddTrailRenderersInChildren(Transform parent, List<TrailRenderer> list)
    {
        foreach (Transform child in parent)
        {
            if (child.GetComponent<TrailRenderer>() != null)
            {
                list.AddRange(child.GetComponents<TrailRenderer>());
            }
            AddTrailRenderersInChildren(child, list);
        }
    }
}
