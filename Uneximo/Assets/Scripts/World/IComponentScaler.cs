﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Interface for scaling scalable components of world gameobject.
/// </summary>
public interface IComponentScaler
{
    void Scale(float scale);
}
