﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interactable item effect that can set a parameter in target animator. 
/// Useful for linking two items that do different things depending on the other's animator state. 
/// </summary>
public class InteractableItemAnimatorTrigger : MonoBehaviour, IInteractableItemEffect
{
    /// <summary>
    /// Target Animator where the parameter is changed
    /// </summary>
    [Tooltip("Target Animator where the parameter is changed")]
    public Animator target;
    
    /// <summary>
    /// If true, TriggerAnimator must be called from event trigger in animation.  
    /// If false, automatically triggers at the end of interaction. 
    /// </summary>
    [Tooltip("If true, TriggerAnimator must be called from event trigger in animation. If false, automatically triggers at the end of interaction.")]
    public bool customTriggerTime = true;

    /// <summary>
    /// Name of the parameter in target animator
    /// </summary>
    [Tooltip("Name of the parameter in target animator")]
    public string animatorParameterName;
    /// <summary>
    /// Type of the parameter in target animator
    /// </summary>
    [Tooltip("Type of the parameter in target animator")]
    public AnimatorControllerParameterType parameterType = AnimatorControllerParameterType.Trigger;
    /// <summary>
    /// Integer value set to parameter if parameter type is Int
    /// </summary>
    [Tooltip("Integer value set to parameter if parameter type is Int")]
    public int intValue;
    /// <summary>
    /// Bool value set to parameter if parameter type is Bool
    /// </summary>
    [Tooltip("Bool value set to parameter if parameter type is Bool")]
    public bool boolValue;
    /// <summary>
    /// Float value set to parameter if parameter type is Float
    /// </summary>
    [Tooltip("Float value set to parameter if parameter type is Float")]
    public float floatValue;

    /// <summary>
    /// Sets parameter in target animator. Parameter and its value is defined in public variables. 
    /// </summary>
    public void TriggerAnimator()
    {
        if (target == null)
        {
            Debug.LogWarning("Target animator not set in InteractableItemAnimatorTrigger");
            return;
        }

        switch(parameterType)
        {
            case AnimatorControllerParameterType.Bool:
                target.SetBool(animatorParameterName, boolValue);
                break;
            case AnimatorControllerParameterType.Float:
                target.SetFloat(animatorParameterName, floatValue);
                break;
            case AnimatorControllerParameterType.Int:
                target.SetInteger(animatorParameterName, intValue);
                break;
            case AnimatorControllerParameterType.Trigger:
                target.SetTrigger(animatorParameterName);
                break;
            default:
                Debug.LogWarning("Parameter type not set in InteractableItemAnimatorTrigger");
                break;
        }
    }
    
    public void TriggerAnimatorBoolTrue()
    {
        if (target == null)
        {
            Debug.LogWarning("Target animator not set in InteractableItemAnimatorTrigger");
            return;
        }

        target.SetBool(animatorParameterName, true);
    }
    
    public void TriggerAnimatorBoolFalse()
    {
        if (target == null)
        {
            Debug.LogWarning("Target animator not set in InteractableItemAnimatorTrigger");
            return;
        }

        target.SetBool(animatorParameterName, false);
    }
    
    public void TriggerAnimatorInt(int intParam)
    {
        if (target == null)
        {
            Debug.LogWarning("Target animator not set in InteractableItemAnimatorTrigger");
            return;
        }

        target.SetInteger(animatorParameterName, intParam);
    }
    
    public void TriggerAnimatorFloat(float floatParam)
    {
        if (target == null)
        {
            Debug.LogWarning("Target animator not set in InteractableItemAnimatorTrigger");
            return;
        }

        target.SetFloat(animatorParameterName, floatParam);
    }
    
    public void TriggerAnimatorTrigger()
    {
        if (target == null)
        {
            Debug.LogWarning("Target animator not set in InteractableItemAnimatorTrigger");
            return;
        }

        target.SetTrigger(animatorParameterName);
    }

    public void InteractingStopped()
    {
        if (!customTriggerTime)
        {
            // TriggerAnimator();
        }
    }

    public void InteractingStarted()
    {
        
    }
}
