﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Implements IComponentScaler. This script is added to world's root gameobject.
/// Called from WorldNavigator, it scales audio sources of its child objects.
/// </summary>
public class AudioSourceScaler : MonoBehaviour, IComponentScaler
{
    private const string IDLE_AUDIO_SOURCE_TAG = "ItemIdleAudioSource";
    private const string INTERACT_AUDIO_SOURCE_TAG = "ItemInteractAudioSource";

    /// <summary>
    /// Min distance start value for idle sound audio sources.
    /// </summary>
    public float startIdleSoundMinDistance = 0.01f;
    /// <summary>
    /// Max distance start value for idle sound audio sources. 
    /// </summary>
    public float startIdleSoundMaxDistance = 5f;
    /// <summary>
    /// Min distance start value for interaction sound audio sources.
    /// </summary>
    public float startInteractSoundMinDistance = 0.01f;
    /// <summary>
    /// Max distance start value for interaction sound audio sources.
    /// </summary>
    public float startInteractSoundMaxDistance = 5f;
    /// <summary>
    /// List of idle sound audio sources.
    /// </summary>
    public List<AudioSource> idleAudioSources = new List<AudioSource>();
    /// <summary>
    /// List of interaction sound audio sources.
    /// </summary>
    public List<AudioSource> interactAudioSources = new List<AudioSource>();

    /// <summary>
    /// Initializes idle and interaction audio source lists.
    /// </summary>
    /// <returns>Total count of AudioSources added to idle and interact lists</returns>
    public int FindAudioSources()
    {
        return FindAudioSourcesDirectly();
        // return FindAudioSourcesThroughItemSounds();
    }

    /// <summary>
    /// Finds all AudioSources and sorts them by tag
    /// </summary>
    /// <returns>Total count of AudioSources added to idle and interact lists</returns>
    private int FindAudioSourcesDirectly()
    {
        List<AudioSource> allAudioSources = new List<AudioSource>();

        AddAudioSourcesInChildren(transform, allAudioSources);

        if (idleAudioSources == null)
        {
            idleAudioSources = new List<AudioSource>();
        }
        idleAudioSources.Clear();

        if (interactAudioSources == null)
        {
            interactAudioSources = new List<AudioSource>();
        }
        interactAudioSources.Clear();

        // Sort audio sources to idle and interact
        foreach(AudioSource audioSource in allAudioSources)
        {
            if (audioSource.CompareTag(IDLE_AUDIO_SOURCE_TAG))
            {
                idleAudioSources.Add(audioSource);
            }
            else if (audioSource.CompareTag(INTERACT_AUDIO_SOURCE_TAG))
            {
                interactAudioSources.Add(audioSource);
            }
        }

        return idleAudioSources.Count + interactAudioSources.Count;
    }

    /// <summary>
    /// Finds InteractableItemSound scripts and collects their idle and interact AudioSources
    /// </summary>
    /// <returns>Total count of AudioSources added to idle and interact lists</returns>
    private int FindAudioSourcesThroughItemSounds()
    {
        List<InteractableItemSound> itemSounds = new List<InteractableItemSound>();

        AddItemSoundsInChildren(transform, itemSounds);
        
        if (idleAudioSources == null)
        {
            idleAudioSources = new List<AudioSource>();
        }
        idleAudioSources.Clear();

        if (interactAudioSources == null)
        {
            interactAudioSources = new List<AudioSource>();
        }
        interactAudioSources.Clear();

        foreach(var itemSound in itemSounds)
        {
            if (itemSound.idleAudioSource != null)
            {
                idleAudioSources.Add(itemSound.idleAudioSource);
            }

            if (itemSound.interactAudioSource != null)
            {
                interactAudioSources.Add(itemSound.interactAudioSource);
            }
        }

        return idleAudioSources.Count + interactAudioSources.Count;
    }

    /// <summary>
    /// Finds all InteractableItemSound scripts in hierarchy under given parent transform
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="list"></param>
    private void AddItemSoundsInChildren(Transform parent, List<InteractableItemSound> list)
    {
        foreach (Transform child in parent)
        {
            if (child.GetComponent<InteractableItemSound>() != null)
            {
                list.Add(child.GetComponent<InteractableItemSound>());
            }
            AddItemSoundsInChildren(child, list);
        }
    }

    /// <summary>
    /// Finds all AudioSources in hierarchy under given parent transform
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="list"></param>
    private void AddAudioSourcesInChildren(Transform parent, List<AudioSource> list)
    {
        foreach (Transform child in parent)
        {
            if (child.GetComponent<AudioSource>() != null)
            {
                list.AddRange(child.GetComponents<AudioSource>());
            }
            AddAudioSourcesInChildren(child, list);
        }
    }

    /// <summary>
    /// Called from WorldNavigator, scales audio sources.
    /// </summary>
    /// <param name="scale"></param>
    public void Scale(float scale)
    {
        foreach (AudioSource audioSource in idleAudioSources)
        {
            audioSource.minDistance = scale * startIdleSoundMinDistance;
            audioSource.maxDistance = scale * startIdleSoundMaxDistance;
        }

        foreach(AudioSource audioSource in interactAudioSources)
        {
            audioSource.minDistance = scale * startInteractSoundMinDistance;
            audioSource.maxDistance = scale * startInteractSoundMaxDistance;
        }
    }
}
