﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Attach to Interactable Items that have a different interaction depending on whether player has a key or not
/// </summary>
public class InteractableItemLock : MonoBehaviour
{
    /// <summary>
    /// Key that is required for unlock
    /// </summary>
    [Tooltip("Key that is required for unlock")]
    public InteractableItemKey requiredKey;

    private bool lockActive = false;

    /// <summary>
    /// Checks if player has Required Key
    /// </summary>
    /// <returns>True if unlocked, false if locked</returns>
    public bool TryUnlock()
    {
        if (KeyInventory.Instance == null)
        {
            Debug.LogError("No KeyInventory found in scene");
            return false;
        }

        if (!lockActive)
        {
            return true;
        }

        bool hasKey = KeyInventory.Instance.PlayerHasKey(requiredKey);
        if (hasKey == true)
        {
            KeyInventory.Instance.UseKey(requiredKey, transform.position);
        }
        return hasKey;
    }

    public bool IsLockActive()
    {
        return lockActive;
    }

    public void SetLockActive(bool active)
    {
        lockActive = active;
    }
}
