﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Attach to objects in the scene that react to player interaction with animation, sound or particles
/// </summary>
public class InteractableItem : MonoBehaviour
{
    private const string ANIMATOR_INTERACT_TRIGGER_KEY = "ActionTrigger";
    private const string ANIMATOR_IDLE_SPEED_KEY = "IdleSpeed";
    private const string ANIMATOR_LOCKED_KEY = "Locked";

    /// <summary>
    /// Animator, whose trigger named INTERACT_ANIMATOR_TRIGGER_KEY's value is set when interacted
    /// </summary>
    public Animator animator;
    /// <summary>
    /// Particle system that is played when interacted
    /// </summary>
    public ParticleSystem particles;

    /// <summary>
    /// If true, particle trigger must be set in animation. If false, particles will play after \"Particles Delay\" seconds.
    /// </summary>
    [Tooltip("If true, particle trigger must be set in animation. If false, particles will play after \"Particles Delay\" seconds.")]
    public bool customParticlesTrigger = false;

    /// <summary>
    /// Particles will play after this many seconds if \"Custom Particles Trigger\" is false.
    /// </summary>
    [Tooltip("Particles will play after this many seconds if \"Custom Particles Trigger\" is false.")]
    public float particlesDelay = 0f;

    /// <summary>
    /// True if any interaction system is playing. False if all interaction systems are not playing.
    /// </summary>
    public bool interacting
    {
        get
        {
            return animationInteracting || particlesInteracting;
        }
    }

    /// <summary>
    /// True if animation is playing, false if animation is not playing (Not yet started or is finished)
    /// </summary>
    private bool animationInteracting = false;
    /// <summary>
    /// True if particle system is playing, false if particle system is not playing (Not yet started or is finished)
    /// </summary>
    private bool particlesInteracting = false;

    private InteractableItemLock itemLock;

    private List<IInteractableItemEffect> interactionEffects = new List<IInteractableItemEffect>();

    // private StickerGiver stickerGiver;
    // private InteractableItemKeyGiver keyGiver;

    private Coroutine currentIdleSpeedLerp;

    private void Awake()
    {
        // Tries to get interactable components,
        // they will stay null if not found
        itemLock = GetComponent<InteractableItemLock>();

        // Find all InteractableItemEffects
        interactionEffects = new List<IInteractableItemEffect>(GetComponents<IInteractableItemEffect>());
        // stickerGiver = GetComponent<StickerGiver>();
        // keyGiver = GetComponent<InteractableItemKeyGiver>();
    }

    /// <summary>
    /// Returns true if no interaction elements are playing, false if any interaction element is still playing
    /// </summary>
    /// <returns></returns>
    public bool IsInteractable()
    {
        return !interacting;
    }

    /// <summary>
    /// Called when animation has finished
    /// </summary>
    public void AnimationInteractionStopped()
    {
        animationInteracting = false;
        if (!interacting)
        {
            InteractionsAllStopped();
        }
    }

    /// <summary>
    /// Called when all interact elements have stopped. Triggers any possible IInteractableEffect components attached to this gameObject
    /// </summary>
    private void InteractionsAllStopped()
    {
        if (interactionEffects != null && interactionEffects.Count > 0)
        {
            foreach (IInteractableItemEffect effect in interactionEffects)
            {
                if (effect != null)
                {
                    effect.InteractingStopped();
                }
            }
        }
        WorldManager.Instance.InteractionEnded();

        // if (stickerGiver != null)
        // {
        //     stickerGiver.InteractingStopped();
        // }

        // if (keyGiver != null)
        // {
        //     keyGiver.InteractingStopped();
        // }
    }

    /// <summary>
    /// Interact with item if not interacting (elements playing) already. 
    /// Checks for lock and tries to unlock if necessary. 
    /// </summary>
    public void Interact()
    {
        if (interacting)
        {
            return;
        }
        WorldManager.Instance.InteractionStarted();

        bool lockDelay = false;

        // Check if locked
        if (itemLock != null)
        {
            // Check if lock is currently active (in current animator state)
            if (itemLock.IsLockActive())
            {
                // Try to unlock lock
                if (itemLock.TryUnlock())
                {
                    animator.SetBool(ANIMATOR_LOCKED_KEY, false);
                    lockDelay = true;
                    itemLock.SetLockActive(false);
                }
                else
                {
                    animator.SetBool(ANIMATOR_LOCKED_KEY, true);
                }
            }
        }

        if (!lockDelay)
        {
            TriggerInteraction();
        }
        else
        {
            // If item was locked and now unlocked, wait for UI key use animation to finish first
            StartCoroutine(LockDelaySequence());
        }
    }

    IEnumerator LockDelaySequence()
    {
        yield return new WaitForSeconds(KeyInventory.Instance.keyUseUiDelay);
        TriggerInteraction();
    }

    /// <summary>
    /// Activates all non-null interact elements (animator, particle system) 
    /// and tells any effects interaction started. 
    /// </summary>
    private void TriggerInteraction()
    {
        // Trigger animation
        if (animator != null)
        {
            animationInteracting = true;
            animator.SetTrigger(ANIMATOR_INTERACT_TRIGGER_KEY);
        }

        // Trigger particles if not set to trigger from animation at custom time
        if (particles != null && !customParticlesTrigger)
        {
            particlesInteracting = true;
            StartCoroutine(WaitForParticles());
        }

        // Tell all effects interacting started
        if (interactionEffects != null && interactionEffects.Count > 0)
        {
            foreach (IInteractableItemEffect effect in interactionEffects)
            {
                if (effect != null)
                {
                    effect.InteractingStarted();
                }
            }
        }

        // If there is no animator or particles, end interaction
        if (animator == null && particles == null)
        {
            animationInteracting = false;
            particlesInteracting = false;
            InteractionsAllStopped();
        }
    }

    /// <summary>
    /// Waits for particlesDelay seconds, then plays particles. Waits until particles finished and then sets particlesInteracting to false.
    /// </summary>
    /// <returns></returns>
    IEnumerator WaitForParticles()
    {
        yield return new WaitForSeconds(particlesDelay);
        PlayParticles();

        WaitForEndOfFrame frame = new WaitForEndOfFrame();

        while (true)
        {
            yield return frame;
            if (!particles.IsAlive())
            {
                particlesInteracting = false;
                if (!interacting)
                {
                    InteractionsAllStopped();
                }
                break;
            }
        }
    }

    /// <summary>
    /// Add as a trigger event to an animation to pause the idle animation. 
    /// ANIMATOR_IDLE_SPEED_KEY's value must be set as Idle state's speed multiplier parameter in animator.
    /// ResumeIdleAnimation to resume.
    /// </summary>
    /// <param name="stopTime">Time over which speed falls to zero</param>
    public void PauseIdleAnimation(float stopTime)
    {
        if (stopTime == 0f)
        {
            animator.SetFloat(ANIMATOR_IDLE_SPEED_KEY, 0f);
        }
        else
        {
            if (currentIdleSpeedLerp != null)
            {
                StopCoroutine(currentIdleSpeedLerp);
            }

            currentIdleSpeedLerp = StartCoroutine(LerpIdleSpeed(1f, 0f, stopTime));
        }
    }

    /// <summary>
    /// Add as a trigger event to an animation to resume the idle animation. 
    /// ANIMATOR_IDLE_SPEED_KEY's value must be set as Idle state's speed multiplier  parameter in animator.
    /// </summary>
    /// /// <param name="stopTime">Time over which speed goes back to 1</param>
    public void ResumeIdleAnimation(float resumeTime)
    {
        if (resumeTime == 0f)
        {
            animator.SetFloat(ANIMATOR_IDLE_SPEED_KEY, 1f);
        }
        else
        {
            if (currentIdleSpeedLerp != null)
            {
                StopCoroutine(currentIdleSpeedLerp);
            }

            currentIdleSpeedLerp = StartCoroutine(LerpIdleSpeed(0f, 1f, resumeTime));
        }
    }

    /// <summary>
    /// Lerps animators ANIMATOR_IDLE_SPEED_KEY from startValue to endValue over lerpTime
    /// </summary>
    /// <param name="startValue"></param>
    /// <param name="endValue"></param>
    /// <param name="lerpTime"></param>
    IEnumerator LerpIdleSpeed(float startValue, float endValue, float lerpTime)
    {
        if (lerpTime == 0f)
        {
            animator.SetFloat(ANIMATOR_IDLE_SPEED_KEY, endValue);
            yield break;
        }

        float timer = 0f;
        WaitForEndOfFrame frame = new WaitForEndOfFrame();

        while (timer < lerpTime)
        {
            animator.SetFloat(ANIMATOR_IDLE_SPEED_KEY, Mathf.Lerp(startValue, endValue, timer / lerpTime));

            yield return frame;
            timer += Time.deltaTime;
        }

        animator.SetFloat(ANIMATOR_IDLE_SPEED_KEY, endValue);
    }

    /// <summary>
    /// Add as a trigger event to an animation to start playing particles
    /// </summary>
    public void PlayParticles()
    {
        if (particles != null)
        {
            particles.Play();
        }
        else
        {
            Debug.LogError("Particles (particle system) not set in InteractableItem");
        }
    }

    /// <summary>
    /// Add as a trigger event to an animation to stop playing looping particles
    /// </summary>
    public void StopParticles()
    {
        if (particles != null)
        {
            particles.Stop();
        }
        else
        {
            Debug.LogError("Particles (particle system) not set in InteractableItem");
        }
    }
}
