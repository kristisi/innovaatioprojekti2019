﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is attached to a gameobject that visualizes targeting the camera to an object which can be interacted
/// by looking at it.
/// </summary>
public class InteractLookReticle : MonoBehaviour
{
    // Make sure animator has parameters named exactly the same
    private const string ANIMATOR_TARGET_ACQUIRED_TRIGGER_KEY = "TargetAcquiredTrigger";
    private const string ANIMATOR_INTERACT_TRIGGER_KEY = "InteractTrigger";
    private const string ANIMATOR_TARGET_LOST_TRIGGER_KEY = "TargetLostTrigger";

    /// <summary>
    /// The reticle animator
    /// </summary>
    public Animator animator;

    /// <summary>
    /// Target indicator gameobject.
    /// </summary>
    public GameObject reticle;

    private bool targeting = false;

    /// <summary>
    /// Starts the targeting animation
    /// </summary>
    public void StartTargeting()
    {
        if (!targeting)
        {
            animator.SetTrigger(ANIMATOR_TARGET_ACQUIRED_TRIGGER_KEY);
            targeting = true;
        }
    }

    /// <summary>
    /// Resets animation to idle
    /// </summary>
    public void ResetTargeting()
    {
        if (targeting)
        {
            animator.SetTrigger(ANIMATOR_TARGET_LOST_TRIGGER_KEY);
            targeting = false;
        }
    }

    /// <summary>
    /// Triggers animation moving to the finish state
    /// </summary>
    public void FinishTargeting()
    {
        if (targeting)
        {
            animator.SetTrigger(ANIMATOR_INTERACT_TRIGGER_KEY);
            targeting = false;
        }
    }

    /// <summary>
    /// Sets reticle gameobject active.
    /// </summary>
    public void Show()
    {
        if (!reticle.activeSelf)
        {
            reticle.SetActive(true);
        }
    }
    /// <summary>
    /// Sets reticle gameobject inactive.
    /// </summary>
    public void Hide()
    {
        reticle.SetActive(false);
    }
}
