﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
/// <summary>
/// Handles player touch inputs and transmit them as events.
/// </summary>
public class PlayerInput : MonoBehaviour
{
    /// <summary>
    /// A singleton instance of PlayerInput.
    /// </summary>
    public static PlayerInput Instance = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Event for touch inputs, the touch is sent as parameter
    /// </summary>
    /// <param name="touch">UnityEngine.Touch</param>
    public delegate void TouchEventHandler(Touch touch);
    /// <summary>
    /// An event with float parameter.
    /// </summary>
    /// <param name="value"></param>
    public delegate void FloatEventHandler(float value);
    /// <summary>
    /// An event for rotation, with two float value parameters.
    /// </summary>
    /// <param name="angle"></param>
    /// <param name="distance"></param>
    public delegate void RotateEventHandler(float angle, float distance);
    /// <summary>
    /// An event with bool variable.
    /// </summary>
    /// <param name="value"></param>
    public delegate void BoolEventHandler(bool value);

    /// <summary>
    /// Event is raised when a touch begins
    /// </summary>
    public event TouchEventHandler TouchBegan;
    /// <summary>
    /// Event is raised when a touch moves
    /// </summary>
    public event TouchEventHandler TouchMoved;
    /// <summary>
    /// Event is raised when a touch on UI moves
    /// </summary>
    public event TouchEventHandler TouchMovedUI;
    /// <summary>
    /// Event is raised when a touch ends (finger is lifted)
    /// </summary>
    public event TouchEventHandler TouchEnded;
    /// <summary>
    /// Event is raised when a touch ends on UI element (finger is lifted)
    /// </summary>
    public event TouchEventHandler TouchEndedUI;
    /// <summary>
    /// Event is raised when two touches are detected.
    /// </summary>
    public event FloatEventHandler PinchTouch;
    /// <summary>
    /// Event is raised when two touches are detected.
    /// </summary>
    public event FloatEventHandler PinchTouchUI;
    /// <summary>
    /// Event is raised when two touches are detected.
    /// </summary>
    public event RotateEventHandler RotateTouchUI;
    /// <summary>
    /// Event is raised when pinch movement starts or ends.
    /// </summary>
    public event BoolEventHandler PinchStatusChange;

    private Vector3 previousMousePos;

    private bool touchingUI = false;

    private bool pinchIsOn = false;

    /// <summary>
    /// Raises TouchBegan event
    /// </summary>
    /// <param name="touch">The touch that began</param>
    protected virtual void OnTouchBegan(Touch touch)
    {
        if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
        {
            touchingUI = true;
        }

        if (touchingUI)
        {
            return;
        }

        if (TouchBegan != null)
        {
            TouchBegan(touch);
        }
    }

    /// <summary>
    /// Raises TouchMoved event
    /// </summary>
    /// <param name="touch">The touch that moved</param>
    protected virtual void OnTouchMoved(Touch touch)
    {
        if (touchingUI || pinchIsOn)
        {
            //TouchMovedUI(touch);
            return;
        }

        if (TouchMoved != null)
        {
            TouchMoved(touch);
        }
    }

    /// <summary>
    /// Raises TouchEnded event
    /// </summary>
    /// <param name="touch">The touch that ended</param>
    protected virtual void OnTouchEnded(Touch touch)
    {
        if (touchingUI)
        {
            // // Alternative 1: Check if any other touches are active, only set touchingUI to false when last touch ends
            // bool otherTouches = false;
            // foreach(Touch otherTouch in Input.touches)
            // {
            //     if (!otherTouch.Equals(touch))
            //     {
            //         otherTouches = true;
            //     }
            // }

            // if (!otherTouches)
            // {
            //     touchingUI = false;
            // }
            // return;

            // Alternative 2: Any touch that ends sets touchingUI to false
            //TouchEndedUI(touch);
            touchingUI = false;
            return;
        }

        if (TouchEnded != null)
        {
            TouchEnded(touch);
        }
    }

    /// <summary>
    /// Raises PinchTouch event.
    /// </summary>
    /// <param name="deltaMagnitude">Pinch magnitude change from the previous frame.</param>
    protected virtual void OnPinchTouch(float deltaMagnitude)
    {
        if (touchingUI)
        {
            if(PinchTouchUI != null)
            {
                PinchTouchUI(deltaMagnitude);
            }
            return;
        }

        if (PinchTouch != null)
        {
            PinchTouch(deltaMagnitude);
        }
    }

    /// <summary>
    /// Raises Pinch Rotate event.
    /// </summary>
    /// <param name="touchAngle">Pinch rotation change from the previous frame.</param>
    /// <param name="vectorLength">Distance between two fingers.</param>
    protected virtual void OnRotateTouch(float touchAngle, float vectorLength)
    {
        if (touchingUI)
        {
            if(RotateTouchUI != null)
            {
                RotateTouchUI(touchAngle, vectorLength);
            }
            return;
        }
    }
    /// <summary>
    /// Raises PinchStatusChange event.
    /// </summary>
    /// <param name="status"></param>
    protected virtual void OnPinchStatusChange(bool status)
    {
        if (PinchStatusChange != null)
        {
            PinchStatusChange(status);
        }
    }

    private void Update()
    {
#if UNITY_EDITOR
        HandleMouseEvents();
#endif
        HandleTouchEvents();
    }

    void HandleMouseEvents()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Touch touch = new Touch();
            touch.fingerId = -1;
            touch.position = Input.mousePosition;
            touch.phase = TouchPhase.Began;
            OnTouchBegan(touch);
        }
        else if (Input.GetMouseButton(0) && Input.mousePosition != previousMousePos)
        {
            Touch touch = new Touch();
            touch.fingerId = -1;
            touch.position = Input.mousePosition;
            touch.phase = TouchPhase.Moved;
            touch.deltaPosition = Input.mousePosition - previousMousePos;
            OnTouchMoved(touch);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            Touch touch = new Touch();
            touch.fingerId = -1;
            touch.position = Input.mousePosition;
            touch.phase = TouchPhase.Ended;
            OnTouchEnded(touch);
        }
        else if (Input.GetMouseButton(1))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                touchingUI = true;
            }
            Vector3 mouseMove = Input.mousePosition - previousMousePos;
            float delta = mouseMove.magnitude;
            if (mouseMove.x < 0)
            {
                delta *= -1f;
            }
            OnPinchTouch(delta);
            OnRotateTouch(mouseMove.y, 1);
        }

        previousMousePos = Input.mousePosition;
    }

    void HandleTouchEvents()
    {
        if (pinchIsOn && Input.touchCount != 2)
        {
            pinchIsOn = false;
            OnPinchStatusChange(pinchIsOn);
        }

        if (Input.touchCount == 0)
        {
            return;
        }

        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                {
                    touchingUI = true;
                }
            }
        }

        if (Input.touchCount == 2)
        {
            if (!pinchIsOn)
            {
                pinchIsOn = true;
                OnPinchStatusChange(pinchIsOn);
                return;
            }

            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
            Vector2 currentTouchVector = touchZero.position - touchOne.position;
            Vector2 previousTouchVector = touchZeroPrevPos - touchOnePrevPos;
            float deltaMagnitude = (touchZero.position - touchOne.position).magnitude - (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float currentAngle = Mathf.Atan2(currentTouchVector.y, currentTouchVector.x) * Mathf.Rad2Deg;
            float previousAngle = Mathf.Atan2(previousTouchVector.y, previousTouchVector.x) * Mathf.Rad2Deg;

            float currentMagnitude = touchZero.position.magnitude - touchOne.position.magnitude;
            float deltaAngle = previousAngle - currentAngle;

            OnPinchTouch(deltaMagnitude);
            OnRotateTouch(deltaAngle, currentMagnitude);
            return;
        }

        foreach (Touch touch in Input.touches)
        {
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    OnTouchBegan(touch);
                    break;
                case TouchPhase.Moved:
                    OnTouchMoved(touch);
                    break;
                case TouchPhase.Ended:
                    OnTouchEnded(touch);
                    break;
            }
        }
    }
}
