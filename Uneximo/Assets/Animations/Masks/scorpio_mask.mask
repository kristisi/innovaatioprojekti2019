%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: scorpio_mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: scorpio
    m_Weight: 1
  - m_Path: scorpio/mandibles_left
    m_Weight: 1
  - m_Path: scorpio/mandibles_right
    m_Weight: 1
  - m_Path: scorpio/scorpio_left_claw_joint
    m_Weight: 1
  - m_Path: scorpio/scorpio_left_claw_joint/scorpio_left_claw_1
    m_Weight: 1
  - m_Path: scorpio/scorpio_left_claw_joint/scorpio_left_claw_1/scorpio_left_claw_2
    m_Weight: 1
  - m_Path: scorpio/scorpio_left_leg_1
    m_Weight: 1
  - m_Path: scorpio/scorpio_left_leg_2
    m_Weight: 1
  - m_Path: scorpio/scorpio_left_leg_3
    m_Weight: 1
  - m_Path: scorpio/scorpio_right_claw_joint
    m_Weight: 1
  - m_Path: scorpio/scorpio_right_claw_joint/scorpio_right_claw_1
    m_Weight: 1
  - m_Path: scorpio/scorpio_right_claw_joint/scorpio_right_claw_1/scorpio_right_claw_2
    m_Weight: 1
  - m_Path: scorpio/scorpio_right_leg_1
    m_Weight: 1
  - m_Path: scorpio/scorpio_right_leg_2
    m_Weight: 1
  - m_Path: scorpio/scorpio_right_leg_3
    m_Weight: 1
  - m_Path: scorpio_head
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_l_1_leg_base
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_l_1_leg_base/scorpio_l_1_knee
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_l_1_leg_base/scorpio_l_1_knee/scorpio_l_1_leg_end1
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_l_2_leg_base
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_l_2_leg_base/scorpio_l_2_knee
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_l_2_leg_base/scorpio_l_2_knee/scorpio_l_1_leg_end
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_l_3_leg_base
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_l_3_leg_base/scorpio_l_3_knee
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_l_3_leg_base/scorpio_l_3_knee/scorpio_l_3_leg_end
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_l_arm
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_l_arm/scorpio_l_scissor
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_l_arm/scorpio_l_scissor/scorpio_l_scissor_end
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_r_1_leg_base
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_r_1_leg_base/scorpio_r_1_knee
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_r_1_leg_base/scorpio_r_1_knee/scorpio_r_1_leg_end
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_r_2_leg_base
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_r_2_leg_base/scorpio_r_2_knee
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_r_2_leg_base/scorpio_r_2_knee/scorpio_r_1_leg_end1
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_r_3_leg_base
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_r_3_leg_base/scorpio_r_3_knee
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_r_3_leg_base/scorpio_r_3_knee/scorpio_r_3_leg_end
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_r_arm
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_r_arm/scorpio_r_scissor
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_r_arm/scorpio_r_scissor/scorpio_r_scissor_end
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_spine_1
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_spine_1/scorpio_spine_2
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_spine_1/scorpio_spine_2/scorpio_spine_3
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_spine_1/scorpio_spine_2/scorpio_spine_3/scorpio_spine_4
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_spine_1/scorpio_spine_2/scorpio_spine_3/scorpio_spine_4/scorpio_spine_5
    m_Weight: 1
  - m_Path: scorpio_head/scorpio_root/scorpio_spine_1/scorpio_spine_2/scorpio_spine_3/scorpio_spine_4/scorpio_spine_5/scorpio_spine_end
    m_Weight: 1
  - m_Path: star_1
    m_Weight: 0
  - m_Path: star_2
    m_Weight: 0
