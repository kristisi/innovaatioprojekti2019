﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interactable Item Effect that increments an item counter
/// </summary>
public class InteractableItemCounterIncrementor : MonoBehaviour, IInteractableItemEffect
{
    /// <summary>
    /// Animator trigger key name for when counter is finished
    /// </summary>
    public const string ANIMATOR_COUNTER_FINISHED_KEY = "CounterFinished";
    /// <summary>
    /// Counter that will be incremented
    /// </summary>
    [Tooltip("Counter that will be incremented")]
    public InteractableItemCounter counter;

    /// <summary>
    /// Transform where the counter notification will spawn
    /// </summary>
    [Tooltip("Transform where the counter notification will spawn")]
    public Transform notificationSpawnPosition;

    /// <summary>
    /// If true, GiveKey must be triggered from animation. 
    /// If false, GiveKey is called at the end of the interaction. 
    /// </summary>
    [Tooltip("If true, GiveKey must be triggered from animation. If false, GiveKey is called at the end of the interaction. ")]
    public bool customTriggerTime = false;

    /// <summary>
    /// Tells the InteractableItemCounterManager to increment the counter 
    /// </summary>
    public void IncrementCounter()
    {
        InteractableItemCounterManager.Instance.IncrementCounter(counter, notificationSpawnPosition.position, this);
    }

    /// <summary>
    /// Tells attached InteractableItem's animator that the counter has finished
    /// </summary>
    public void CounterFinished()
    {
        InteractableItem item = GetComponent<InteractableItem>();
        if (item != null)
        {
            item.animator.SetTrigger(ANIMATOR_COUNTER_FINISHED_KEY);
        }
    }

    /// <summary>
    /// Increments counter if customTriggerTime is false
    /// </summary>
    public void InteractingStopped()
    {
        if (!customTriggerTime)
        {
            IncrementCounter();
        }
    }

    public void InteractingStarted()
    {
        
    }
}
