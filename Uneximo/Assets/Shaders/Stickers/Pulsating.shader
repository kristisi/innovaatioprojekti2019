﻿Shader "Sticker/Pulsating" {
	Properties{
		[PerRendererData] _MainTex("Base (RGB)", 2D) = "white" {}
		[Header(Stencil)]
		_StencilComp("Stencil Comparison", Float) = 8
		_Stencil("Stencil ID", Float) = 0
		_StencilOp("Stencil Operation", Float) = 0
		_StencilWriteMask("Stencil Write Mask", Float) = 255
		_StencilReadMask("Stencil Read Mask", Float) = 255
		[Header(Rendering)]
		_ColorMask("Color Mask", Float) = 15
		_Color("Color", Color) = (0.95, 0.93, 0.49, 1)
		_TintAmount("Tint Amount", Range(0, 1)) = 0.5
		_Speed("Pulse Speed", Range(1, 100)) = 20
		_PulseAmount("Pulse Amount", Range (0.1, 1)) = 0.5
	}
		SubShader{
			Tags {"Queue" = "Transparent" "RenderType" = "Transparent"}
			Stencil
			{
				Ref[_Stencil]
				Comp[_StencilComp]
				Pass[_StencilOp]
				ReadMask[_StencilReadMask]
				WriteMask[_StencilWriteMask]
			}
			ZWrite off
			Lighting Off
			ZTest[unity_GUIZTestMode]
			ColorMask[_ColorMask]
			Blend One OneMinusSrcAlpha

			Pass {

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"

				sampler2D _MainTex;

				struct v2f {
					float4 pos : SV_POSITION;
					half2 uv : TEXCOORD0;
				};

				v2f vert(appdata_base v) {
					v2f o;
					o.pos = UnityObjectToClipPos(v.vertex);
					o.uv = v.texcoord;
					return o;
				}

				fixed4 _Color;
				half _TintAmount;
				half _Speed;
				half _PulseAmount;

				fixed4 frag(v2f i) : COLOR
				{
					half4 c = tex2D(_MainTex, i.uv);
					c.rgb = c.rgb*(1 - _TintAmount) + _Color * _TintAmount;
					fixed p = (_PulseAmount + sin(_Time.x*_Speed)*_PulseAmount)/ 2.0;

					c.rgb += p*c.rgb;
					c.rgb *= c.a;

					return c;
				}

				ENDCG
			}
		}
			FallBack "Diffuse"
}
