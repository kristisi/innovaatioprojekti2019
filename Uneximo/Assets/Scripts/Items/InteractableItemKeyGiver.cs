﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interactable item effect that can give a key to a player. 
/// Attach to interactable items that give a key. 
/// </summary>
public class InteractableItemKeyGiver : MonoBehaviour, IInteractableItemEffect
{
    /// <summary>
    /// Key that is given to the player
    /// </summary>
    [Tooltip("Key that is given to the player")]
    public InteractableItemKey key;

    /// <summary>
    /// If true, GiveKey must be triggered from animation. 
    /// If false, GiveKey is called at the end of the interaction. 
    /// </summary>
    [Tooltip("If true, GiveKey must be triggered from animation. If false, GiveKey is called at the end of the interaction. ")]
    public bool customTriggerTime = false;

    public void OpenKeySlot()
    {
        if (KeyInventory.Instance == null)
        {
            Debug.LogError("No KeyInventory found in scene");
            return;
        }

        KeyInventory.Instance.AddKeySlot(key);
    }

    /// <summary>
    /// Gives the key to the player by sending it to KeyInventory
    /// </summary>
    public void GiveKey()
    {
        if (KeyInventory.Instance == null)
        {
            Debug.LogError("No KeyInventory found in scene");
            return;
        }

        KeyInventory.Instance.AddKey(key, transform.position);
    }

    /// <summary>
    /// Gives key if not given yet
    /// </summary>
    public void InteractingStopped()
    {
        if (!customTriggerTime)
        {
            GiveKey();
        }
    }

    public void InteractingStarted()
    {
        
    }
}
