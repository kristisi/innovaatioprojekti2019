﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles initialization and provides access to data serailization and events.
/// </summary>
public class GameController : MonoBehaviour
{
    /// <summary>
    /// Singleton instance of GameController.
    /// </summary>
    public static GameController Instance = null;
    /// <summary>
    /// A value used as a wait counter.
    /// </summary>
    public float wait = 1f;
    /// <summary>
    /// A delegate for WorldManager.WorldID events.
    /// </summary>
    /// <param name="worldID">WorldManager.WorldID</param>
    public delegate void WorldIDEventHandler(WorldManager.WorldID worldID);
    /// <summary>
    /// This event is raised when starts to change world and sends the ID of the new world (WorldManager.WorldID)
    /// </summary>
    public event WorldIDEventHandler WorldChangeStarted;
    /// <summary>
    /// A void delegate.
    /// </summary>
    public delegate void WorldEventHandler();
    /// <summary>
    /// This event is raised when world is activated.
    /// </summary>
    public event WorldEventHandler WorldActivated;
    /// <summary>
    /// This event is raised when world is deactivated.
    /// </summary>
    public event WorldEventHandler WorldDeactivated;
    /// <summary>
    /// This event is raised at the beginning, when game controller has initialized serialized data.
    /// </summary>
    public event WorldEventHandler GameControllerReady;
    /// <summary>
    /// This event is raised when Titlescreen is closed.
    /// </summary>
    public event WorldEventHandler TitlescreenClosed;
    /// <summary>
    /// This event is raised when a sticker is obtained.
    /// </summary>
    public event WorldEventHandler StickerObtained;
    /// <summary>
    /// This event is raised when sticker is placed on stickerbook canvas.
    /// </summary>
    public event WorldEventHandler StickerIsPlaced;
    /// <summary>
    /// This event is raised when sticker is ouside of the dedicated area.
    /// </summary>
    public event WorldEventHandler StickerOutside;
    /// <summary>
    /// This event is raised when a sticker is touched.
    /// </summary>
    public event WorldEventHandler StickerIsTouched;

    /// <summary>
    /// An array of obtained stickers.
    /// </summary>
    public bool[] stickers;
    /// <summary>
    /// A list of placed stickers.
    /// </summary>
    public List<StickerData> stickerData;
    /// <summary>
    /// A data container for miscellaneous seriazable game data.
    /// </summary>
    public MiscData miscData;

    private bool ready = false;

    private IPlayerSaveData playerSaveData;

    private WorldManager.WorldID currentWorldID;


    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
        Application.targetFrameRate = 60;
        playerSaveData = FindObjectOfType<LocalPlayerData>();

    }

    void Start()
    {
        ARController.Instance.ActivateARController();
        //ResetAllSavedData();
        //ResetPlacedStickers();
        //ResetStickers();
        //ResetMiscData();
        LoadDataFromStorage();
        StickerBook.Instance.InitStickers(stickers);
        if (PointerTutorial.Instance != null)
        {
            PointerTutorial.Instance.Init(miscData.pointerTutorialCompleted);
        }
        
        WorldManager.Instance.WorldActivated += OnWorldActivated;
        WorldManager.Instance.WorldDeactivated += OnWorldDeactivated;

    }

    void Update()
    {
        if (!ready)
        {
            wait -= Time.deltaTime;
            if (wait <= 0f)
            {
                ready = true;
                OnGameControllerReady();
            }
        }
    }
    /// <summary>
    /// Loads serialized data of placed stickers.
    /// </summary>
    /// <returns>Data of palced stickers.</returns>
    public List<StickerData> LoadPlacedStickers()
    {
        StickerData[] s = playerSaveData.LoadPlacedStickers();
        List<StickerData> st = new List<StickerData>();
        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == null)
            {
                break;
            }
            else
            {
                st.Add(s[i]);
            }
        }
        return st;
    }
    private void LoadDataFromStorage()
    {
        
        stickers = playerSaveData.LoadStickers();
        stickerData = LoadPlacedStickers();
        miscData = playerSaveData.LoadMiscData();
    }

    /// <summary>
    /// Saves data of placed stickers.
    /// </summary>
    /// <param name="placedStickers">Data of placed stickers.</param>
    public void SavePlacedStickers(List<StickerData> placedStickers)
    {
        StickerData[] st = new StickerData[30];
        for (int i = 0; i < st.Length; i++)
        {
            if (i < placedStickers.Count)
            {
                st[i] = placedStickers[i];
            }
            else
            {
                st[i] = null;
            }
        }
        playerSaveData.SavePlacedStickers(st);
    }
    /// <summary>
    /// Updates data of obtained stickers and saves it.
    /// </summary>
    /// <param name="sticker">Index of the sticker.</param>
    /// <param name="value">Sticker's new obtained status.</param>
    public void UpdateStickers(int sticker, bool value)
    {
        if (sticker < 0)
        {
            // Debug.Log("Tarra puuttuu.");
            return;
        }
        OnStickerObtained();
        if (sticker < stickers.Length)
        {
            stickers[sticker] = value;
            playerSaveData.SaveStickers(stickers);
        }
    }
    /// <summary>
    /// Sends a request for WorldManager to change the current world.
    /// </summary>
    /// <param name="worldToLoad">ID of the loaded world.</param>
    public void LoadLevel(WorldManager.WorldID worldToLoad)
    {
        WorldManager.Instance.ChangeActiveWorld(worldToLoad);

        currentWorldID = worldToLoad;
        //OnWorldChanged(currentWorldID); kusutaan vasta WorldManagerista kun oikeasti vaihtaa
    }
    /// <summary>
    /// Calls ARController to reset ARCore anchor status.
    /// </summary>
    public void ResetWorldPosition()
    {
        ARController.Instance.ResetToSearch();
    }
    /// <summary>
    /// Resets all saved data.
    /// </summary>
    public void ResetAllSavedData()
    {
        playerSaveData.ResetAllSavedData();
    }
    /// <summary>
    /// Resests miscellaneous save data.
    /// </summary>
    public void ResetMiscData()
    {
        playerSaveData.ResetMiscData();
        miscData = playerSaveData.LoadMiscData();
    }
    /// <summary>
    /// Resets both obtained and placed stickers in save data.
    /// </summary>
    public void ResetStickers()
    {
        playerSaveData.ResetStickers();
        playerSaveData.ResetPlacedStickers();
    }
    /// <summary>
    /// Resests saved data of placed stickers.
    /// </summary>
    public void ResetPlacedStickers()
    {
        playerSaveData.ResetPlacedStickers();
    }
    /// <summary>
    /// Access to raise an event when sticker is placed on canvas.
    /// </summary>
    public void StickerPlacedOnCanvas()
    {
        OnStickerPlacedOnCanvas();
    }
    /// <summary>
    /// Access to raise an event when sticker is placed outside of dedicated area on stickerbook canvas.
    /// </summary>
    public void StickerOutOfCanvas()
    {
        OnStickerOutOfCanvas();
    }
    /// <summary>
    /// Access to raise an event when a sticker is touched.
    /// </summary>
    public void StickerTouched()
    {
        OnStickerTouched();
    }
    /// <summary>
    /// saves information that tutorial is completed.
    /// </summary>
    public void PointerTutorialCompleted()
    {
        miscData.pointerTutorialCompleted = true;
        playerSaveData.SaveMiscData(miscData);
    }
    /// <summary>
    /// saves information that start tutorial is completed.
    /// </summary>
    public void StartTutorialCompleted()
    {
        miscData.startTutorialCompleted = true;
        playerSaveData.SaveMiscData(miscData);
    }

    /// <summary>
    /// Raises event TitlescreenClosed.
    /// </summary>
    public void OnTitlescreenClosed()
    {
        if(TitlescreenClosed != null)
        {
            TitlescreenClosed();
        }
    }
    /// <summary>
    /// Raises event StickerOutside.
    /// </summary>
    protected virtual void OnStickerOutOfCanvas()
    {
        if (StickerOutside != null)
        {
            StickerOutside();
        }
    }
    /// <summary>
    /// Raises event StickerIsTouched. 
    /// </summary>
    protected virtual void OnStickerTouched()
    {
        if (StickerIsTouched != null)
        {
            StickerIsTouched();
        }
    }
    /// <summary>
    /// Raises event StickerIsPlaced.
    /// </summary>
    protected virtual void OnStickerPlacedOnCanvas()
    {
        if (StickerIsPlaced != null)
        {
            StickerIsPlaced();
        }
    }
    /// <summary>
    /// Raises event GameControllerReady.
    /// </summary>
    public virtual void OnGameControllerReady()
    {
        if (GameControllerReady != null)
        {
            GameControllerReady();
        }
    }
    /// <summary>
    /// Raises event WorldActivated.
    /// </summary>
    protected virtual void OnWorldActivated()
    {
        if (WorldActivated != null)
        {
            WorldActivated();
        }
    }
    /// <summary>
    /// Raises event WorldDeactivated.
    /// </summary>
    protected virtual void OnWorldDeactivated()
    {
        if (WorldDeactivated != null)
        {
            WorldDeactivated();
        }
    }
    /// <summary>
    /// Raises event StickerObtained.
    /// </summary>
    public virtual void OnStickerObtained()
    {
        if (StickerObtained != null)
        {
            StickerObtained();
        }
    }
    /// <summary>
    /// Raises event WorldChangeStarted with parameter WorldManager.WorldID.
    /// </summary>
    /// <param name="worldID">ID of the next activated world.</param>
    public virtual void OnWorldChangeStarted(WorldManager.WorldID worldID)
    {
        if (WorldChangeStarted != null)
        {
            WorldChangeStarted(worldID);
        }
    }
    /// <summary>
    /// Returns IPlayerSaveData.
    /// </summary>
    /// <returns>The object instance implementing IPlayerSaveData interface.</returns>
    public IPlayerSaveData GetPlayerSaveData()
    {
        return playerSaveData;
    }
    /// <summary>
    /// Get the time when gift was received.
    /// </summary>
    /// <returns>When gift was received most recently.</returns>
    public System.DateTime GetGiftReceivedTime()
    {
        return miscData.giftReceived;
    }
    /// <summary>
    /// Set and save when gift was received. 
    /// </summary>
    /// <param name="newTime">When gift was received.</param>
    public void SetGiftReceivedTime(System.DateTime newTime)
    {
        miscData.giftReceived = newTime;
        playerSaveData.SaveMiscData(miscData);
    }
}
