﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interactable item effect that can play idle and interact sounds for an interactable item. 
/// </summary>
public class InteractableItemSound : MonoBehaviour, IInteractableItemEffect
{
    private const float IDLE_INTERRUPT_FADE_TIME = 0.5f;
    
    /// <summary>
    /// AudioSource for idle sound
    /// </summary>
    public AudioSource idleAudioSource;
    /// <summary>
    /// AudioSource for interact sound
    /// </summary>
    public AudioSource interactAudioSource;

    /// <summary>
    /// If true, idle sound will only play when triggered from animation. 
    /// If false, idle sound will play randomly.
    /// </summary>
    [Tooltip("If true, idle sound will only play when triggered from animation. If false, idle sound will play randomly.")]
    public bool customIdleSoundTime = false;

    /// <summary>
    /// If true, interact sound will only play when triggered from animation. 
    /// If false, interact sound will play at start of interaction.
    /// </summary>
    [Tooltip("If true, interact sound will only play when triggered from animation. If false, interact sound will play at start of interaction.")]
    public bool customInteractSoundTime = false;

    /// <summary>
    /// Minimum delay between idle sound ending and starting again. 
    /// Actual time is random between min and max.
    /// </summary>
    [Tooltip("Minimum delay between idle sound ending and starting again. Actual time is random between min and max.")]
    public float minIdleRandomDelay = 0f;
    /// <summary>
    /// Maximum delay between idle sound ending and starting again. 
    /// Actual time is random between min and max.
    /// </summary>
    [Tooltip("Maximum delay between idle sound ending and starting again. Actual time is random between min and max.")]
    public float maxIdleRandomDelay = 0f;

    private float currentWaitTime = 0f;

    private bool interactingActive = false;

    private bool idleSoundFaded = false;

    private void Start()
    {
        // Initial random delay
        if (!customIdleSoundTime)
        {
            Invoke("PlayIdleSoundAfterRandomDelay", Random.Range(minIdleRandomDelay, maxIdleRandomDelay));
        }
    }

    /// <summary>
    /// Plays idle sound then waits a random delay before recursively invoking itself. 
    /// Delay is between minIdleRandomDelay and maxIdleRandomDelay. 
    /// </summary>
    public void PlayIdleSoundAfterRandomDelay()
    {
        if (customIdleSoundTime)
        {
            return;
        }
        
        if (idleAudioSource != null && idleAudioSource.clip != null)
        {
            // Play idle sound
            if (!interactingActive && !idleAudioSource.isPlaying)
            {
                idleAudioSource.Play();
            }
            
            // Make the next delay random in between the min and max values plus the length of the idle clip
            // so that the delay starts after the clip has played
            currentWaitTime = Random.Range(minIdleRandomDelay, maxIdleRandomDelay) + idleAudioSource.clip.length * idleAudioSource.pitch;
            Invoke("PlayIdleSoundAfterRandomDelay", currentWaitTime);
        }
    }

    /// <summary>
    /// Plays idle sound. 
    /// If added as an event trigger to an animation, set customIdleSoundTime to true. 
    /// </summary>
    public void PlayIdleSound()
    {
        if (idleAudioSource != null && idleAudioSource.clip != null)
        {
            if (!interactingActive && !idleAudioSource.isPlaying)
            {
                idleAudioSource.Play();
            }
        }
    }

    /// <summary>
    /// Stops idle sound if it is playing.
    /// This can be added as an event trigger to an animation.  
    /// </summary>
    public void StopIdleSound()
    {
        if (idleAudioSource != null)
        {
            idleAudioSource.Stop();
        }
    }

    /// <summary>
    /// Fades out idle sound and plays interact sound. 
    /// If added as an event trigger to an animation, set customInteractSoundTime to true. 
    /// </summary>
    public void PlayInteractSound()
    {
        if (idleAudioSource != null && idleAudioSource.isPlaying)
        {
            idleSoundFaded = true;
            StartCoroutine(AudioManager.FadeOutSound(idleAudioSource, IDLE_INTERRUPT_FADE_TIME));
        }
        
        if (interactAudioSource != null)
        {
            interactAudioSource.Play();
        }
        else
        {
            AudioManager.Instance.PlayGenericInteractSound();
        }

        interactingActive = true;
    }

    /// <summary>
    /// Stops interact sound. 
    /// This can be added as an event trigger to an animation. 
    /// </summary>
    public void StopInteractSound()
    {
        if (interactAudioSource != null)
        {
            interactAudioSource.Stop();
        }
    }

    /// <summary>
    /// When interacting starts, plays interact sound if customInteractSoundTime is false
    /// </summary>
    public void InteractingStarted()
    {
        if (!customInteractSoundTime)
        {
            PlayInteractSound();
        }
    }

    /// <summary>
    /// When interacting stops, fades idle sound back in. 
    /// </summary>
    public void InteractingStopped()
    {
        if (idleAudioSource != null && idleSoundFaded)
        {
            StartCoroutine(AudioManager.FadeInSound(idleAudioSource, IDLE_INTERRUPT_FADE_TIME));
        }

        idleSoundFaded = false;

        interactingActive = false;
    }
}
