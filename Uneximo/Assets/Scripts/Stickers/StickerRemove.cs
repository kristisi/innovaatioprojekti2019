﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.EventSystems;
//using UnityEngine.UI;

///// <summary>
///// Removes the sticker when hovered over the sticker container viewport.
///// </summary>
//public class StickerRemove : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
//{
//    //public static StickerRemove instance;
//    /// <summary>
//    /// Flag if the mouse is currently over container viewport.
//    /// </summary>
//    public bool mouseOverStickerList = false;
//    /// <summary>
//    /// Color to highlight when mouse is over the container viewport.
//    /// </summary>
//    public Color highlightColor;
//    /// <summary>
//    ///  Original color of the container viewport.
//    /// </summary>
//    private Color originalColor;
//    /// <summary>
//    /// Container viewport image.
//    /// </summary>
//    private Image imageToChange;

//    //private void Awake()
//    //{
//    //    if (instance == null)
//    //    {
//    //        instance = this;
//    //    }
//    //    else
//    //    {
//    //        Destroy(gameObject);
//    //    }
//    //}

//    private void Start()
//    {
//        // Cache reference to the viewport image.
//        imageToChange = transform.parent.GetComponent<Image>();
//        // Cache the original color.
//        originalColor = imageToChange.color;
//    }

//    /// <summary>
//    /// Changes the color if cursor is dragging and is over the viewport.
//    /// </summary>
//    /// <param name="eventData"></param>
//    public void OnPointerEnter(PointerEventData eventData)
//    {
//        if (!eventData.dragging || eventData.pointerDrag.tag != "PlacedSticker")
//        {
//            return;
//        }
//        mouseOverStickerList = true;
//        eventData.pointerDrag.GetComponent<PlacedSticker>().isOverDestroyerObject = true;
//        imageToChange.color = highlightColor;
//    }

//    /// <summary>
//    /// Reverts the color to normal after cursor leaves the viewport.
//    /// </summary>
//    /// <param name="eventData"></param>
//    public void OnPointerExit(PointerEventData eventData)
//    {
//        if (!eventData.dragging || eventData.pointerDrag.tag != "PlacedSticker")
//        {
//            return;
//        }
//        mouseOverStickerList = false;
//        eventData.pointerDrag.GetComponent<PlacedSticker>().isOverDestroyerObject = false;
//        imageToChange.color = originalColor;
//    }

//    /// <summary>
//    /// Changes color to highlight and triggers tracker flag.
//    /// </summary>
//    public void HighlightArea()
//    {
//        mouseOverStickerList = true;
//        imageToChange.color = highlightColor;
//    }

//    /// <summary>
//    /// Reverts back to original color.
//    /// </summary>
//    public void RemoveHighlight()
//    {
//        imageToChange.color = originalColor;
//    }
//}
