﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles player collected key items
/// </summary>
public class KeyInventory : MonoBehaviour
{
    public static KeyInventory Instance = null;

    /// <summary>
    /// Time that it takes for key use animation to play in UI.
    /// InteractableItem waits for this long when key is used before playing its own interact animation and other elements. 
    /// </summary>
    [Tooltip("Time that it takes for key use animation to play in UI. InteractableItem waits for this long when key is used before playing its own interact animation and other elements. ")]
    public float keyUseUiDelay = 1f;

    private List<InteractableItemKey> collectedKeys = new List<InteractableItemKey>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        WorldManager.Instance.WorldActivated += ClearInventory;
    }

    /// <summary>
    /// Clears inventory and removes all items from player
    /// </summary>
    /// <param name="worldID"></param>
    public void ClearInventory()
    {
        foreach (InteractableItemKey key in collectedKeys)
        {
            InventoryUI.Instance.UseItem(key, Vector3.zero);
        }

        InventoryUI.Instance.ClearItemSlots();

        collectedKeys.Clear();
    }

    /// <summary>
    /// Tells if player has the key
    /// </summary>
    /// <param name="key">Key to find on player</param>
    /// <returns>True if player has key, false if player doesn't have key</returns>
    public bool PlayerHasKey(InteractableItemKey key)
    {
        return collectedKeys.Contains(key);
    }

    /// <summary>
    /// Adds a key slot for the specified key
    /// </summary>
    /// <param name="key">Key to open slot for</param>
    public void AddKeySlot(InteractableItemKey key)
    {
        if (!collectedKeys.Contains(key))
        {
            InventoryUI.Instance.AddItemSlot(key);
        }
    }

    /// <summary>
    /// Adds a key to player
    /// </summary>
    /// <param name="key">Key to add</param>
    /// <param name="itemPosition">World position where key is from</param>
    public void AddKey(InteractableItemKey key, Vector3 itemPosition)
    {
        if (!collectedKeys.Contains(key))
        {
            collectedKeys.Add(key);
            Debug.Log("Added key to inventory: " + key.name);
            InventoryUI.Instance.AddItem(key);
        }
    }

    /// <summary>
    /// Removes key from player inventory and UI
    /// </summary>
    /// <param name="key">Key to remove</param>
    /// <param name="usePosition">World position where key is used</param>
    public void UseKey(InteractableItemKey key, Vector3 usePosition)
    {
        collectedKeys.Remove(key);
        InventoryUI.Instance.UseItem(key, Camera.main.WorldToScreenPoint(usePosition));
    }
}
