﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Manages sticker notifications.
/// </summary>
public class StickerNotificationManager : MonoBehaviour
{
    /// <summary>
    /// A singleton instance of the class.
    /// </summary>
    public static StickerNotificationManager Instance = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        if (endPosition == null || notificationCanvas == null)
        {
            if (endPosition == null)
            {
                Debug.LogWarning("endPosition is null, please assign it in the editor");
            }

            if (notificationCanvas == null)
            {
                Debug.LogWarning("notificationCanvas is null, please assign it in the editor");
            }
            active = false;
        }
    }

    public Transform notificationCanvas;
    public GameObject notificationPrefab;
    public Transform endPosition;
    public Transform waitPosition;

    [HideInInspector]
    public int notificationCount = 0;

    private bool active = true;

    private List<StickerNotification> activeNotifications = new List<StickerNotification>();

    private bool notificationsHidden = false;

    private void Start()
    {
        UIController.Instance.ShowStickerBook += HideActiveNotifications;
        UIController.Instance.HideStickerBook += ShowActiveNotifications;
    }

    /// <summary>
    /// Spawns a notification at position with the icon of sticker
    /// </summary>
    /// <param name="position">Position that notification starts at</param>
    /// <param name="sticker">Sticker that the notication represents</param>
    public void SpawnNotification(Vector3 position, Sticker sticker)
    {
        // Debug.Log("started spawn notification, count: " + notificationCount);
        // Fix random freeze when fourth notification is spawned
        if (notificationCount >= 3)
        {
            return;
        }

        if (!active)
        {
            Debug.LogWarning("Cannot spawn sticker notification because of null references");
            return;
        }

        GameObject notification = Instantiate(notificationPrefab, notificationCanvas);
        Vector3 screenPos = Camera.main.WorldToScreenPoint(position);
        notification.transform.position = screenPos;

        StickerNotification stickerNotification = notification.GetComponent<StickerNotification>();
        stickerNotification.Init(sticker);

        if (!notificationsHidden)
        {
            stickerNotification.Show();
        }

        stickerNotification.StartAnimation(waitPosition.position);
        activeNotifications.Add(stickerNotification);

        notificationCount++;
        // Debug.Log("spawned notification, count: " + notificationCount);
    }

    /// <summary>
    /// Removes notification from active notifications list.
    /// </summary>
    /// <param name="notification"></param>
    public void RetireNotification(StickerNotification notification)
    {
        activeNotifications.Remove(notification);
    }

    private void HideActiveNotifications()
    {
        if (activeNotifications != null && activeNotifications.Count > 0)
        {
            foreach (StickerNotification notification in activeNotifications)
            {
                notification.Hide();
            }
        }

        notificationsHidden = true;
    }

    private void ShowActiveNotifications()
    {
        if (activeNotifications != null && activeNotifications.Count > 0)
        {
            foreach (StickerNotification notification in activeNotifications)
            {
                notification.Show();
            }
        }

        notificationsHidden = false;
    }
}
