﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// A generic data contaner for bool values.
/// </summary>
[Serializable]
public class PlayerBoolData
{
    public bool[] boolData;
}
/// <summary>
/// A data container for saving values of one placed sticker on sticerbook canvas.
/// </summary>
[Serializable]
public class StickerData
{
    public string name;
    public float scale;
    public float rotation;
    public float xCoord;
    public float yCoord;
}
/// <summary>
/// A data container for an array of placed stickers.
/// </summary>
[Serializable]
public class PlacedStickersData
{
    public StickerData[] stickerData;
}
/// <summary>
/// A data container for miscellanious game save data.
/// </summary>
[Serializable]
public class MiscData
{
    public DateTime giftReceived;
    public bool startTutorialCompleted;
    public bool pointerTutorialCompleted;
}

