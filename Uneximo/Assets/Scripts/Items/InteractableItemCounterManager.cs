﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// InteractableItemCounterManager handles item counters and their current counts, 
/// keeps track of incrementors that have incremented a counter and lets them know when their counter is finished,
/// and spawns UI elements to show the counts. 
/// </summary>
public class InteractableItemCounterManager : MonoBehaviour
{
    /// <summary>
    /// Singleton instance for InteractableItemCounterManager
    /// </summary>
    public static InteractableItemCounterManager Instance = null;

    /// <summary>
    /// Transform that is used as parent for counter notifications. 
    /// Must be somewhere under a canvas in hierarchy for the UI elements to work. 
    /// </summary>
    public Transform counterNotificationCanvas;
    /// <summary>
    /// Prefab for item counter UI notifications. 
    /// It must have a ItemCounterUI component. 
    /// </summary>
    public GameObject counterUiPrefab;

    private Dictionary<InteractableItemCounter, int> activeCounters = new Dictionary<InteractableItemCounter, int>();
    private List<ItemCounterUI> activeUiCounters = new List<ItemCounterUI>();

    private Dictionary<InteractableItemCounter, List<InteractableItemCounterIncrementor>> activeCounterIncrementors = new Dictionary<InteractableItemCounter, List<InteractableItemCounterIncrementor>>();

    private bool notificationsHidden = false;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        if (counterNotificationCanvas == null)
        {
            Debug.LogWarning("notificationCanvas is null, please assign it in the editor");
        }
    }

    private void Start()
    {
        UIController.Instance.ShowStickerBook += HideActiveUiCounters;
        UIController.Instance.HideStickerBook += ShowActiveUiCounters;
    }

    /// <summary>
    /// Increment a counter and show a notification
    /// </summary>
    /// <param name="counter">Counter to increment</param>
    /// <param name="position">World position where notification will appear</param>
    public void IncrementCounter(InteractableItemCounter counter, Vector3 position, InteractableItemCounterIncrementor incrementor)
    {
        if (activeCounters.ContainsKey(counter))
        {
            activeCounters[counter]++;
        }
        else
        {
            activeCounters.Add(counter, 1);
        }

        if (activeCounterIncrementors.ContainsKey(counter))
        {
            activeCounterIncrementors[counter].Add(incrementor);
        }
        else
        {
            activeCounterIncrementors.Add(counter, new List<InteractableItemCounterIncrementor>());
            activeCounterIncrementors[counter].Add(incrementor);
        }

        // Spawn notification
        SpawnCounterNotification(counter, position);

        if (activeCounters[counter] >= counter.maxCount)
        {
            FinishCounter(counter, position);
        }
    }

    /// <summary>
    /// Spawns a counter notification at position with the counter count
    /// </summary>
    /// <param name="position">Position for the counter notification</param>
    /// <param name="counter">Counter that the notification is about</param>
    private void SpawnCounterNotification(InteractableItemCounter counter, Vector3 position)
    {
        if (!activeCounters.ContainsKey(counter))
        {
            activeCounters.Add(counter, 0);
            Debug.LogWarning("Spawning notification for uninitialized counter");
        }

        GameObject notification = Instantiate(counterUiPrefab, counterNotificationCanvas);
        Vector3 screenPos = Camera.main.WorldToScreenPoint(position);
        notification.transform.position = screenPos;

        ItemCounterUI counterNotification = notification.GetComponent<ItemCounterUI>();
        if (counterNotification == null)
        {
            Debug.LogError("Unable to spawn counter UI notification: counterUiPrefab in InteractableItemCounterManager doesn't have ItemCounterUI component.");
            Destroy(notification);
            return;
        }
        int max = counter.maxCount;
        int current = activeCounters[counter];
        counterNotification.Init(current, max);

        if (!notificationsHidden)
        {
            counterNotification.ShowWithTransition();
        }

        activeUiCounters.Add(counterNotification);
    }

    /// <summary>
    /// Removes notification from active notifications list.
    /// </summary>
    /// <param name="notification"></param>
    public void RetireNotification(ItemCounterUI notification)
    {
        activeUiCounters.Remove(notification);
    }

    private void FinishCounter(InteractableItemCounter counter, Vector3 position)
    {
        counter.FinishCounter(position);

        activeCounters.Remove(counter);

        // Tell incrementors counter is finished
        foreach(InteractableItemCounterIncrementor incrementor in activeCounterIncrementors[counter])
        {
            incrementor.CounterFinished();
        }
        activeCounterIncrementors[counter].Clear();
        activeCounterIncrementors.Remove(counter);
    }

    private void HideActiveUiCounters()
    {
        if (activeCounters != null && activeCounters.Count > 0)
        {
            foreach (ItemCounterUI notification in activeUiCounters)
            {
                notification.Hide();
            }
        }

        notificationsHidden = true;
    }

    private void ShowActiveUiCounters()
    {
        if (activeCounters != null && activeCounters.Count > 0)
        {
            foreach (ItemCounterUI notification in activeUiCounters)
            {
                notification.Show();
            }
        }

        notificationsHidden = false;
    }
}
