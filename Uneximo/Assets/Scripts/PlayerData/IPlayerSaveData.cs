﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

/// <summary>
/// Interface for data storage classes.
/// </summary>
public interface IPlayerSaveData
{
    /// <summary>
    /// Returns boolean array of activation status of stickers.
    /// </summary>
    /// <returns></returns>
    bool[] LoadStickers();
    /// <summary>
    /// Saves the status of the stickers to data storage.
    /// </summary>
    /// <param name="stickers">Boolean array of the activation status of the stickers.</param>
    void SaveStickers(bool[] stickers);
    /// <summary>
    /// Load data of placed stickers in stickerbook.
    /// </summary>
    /// <returns>Data of stickers placed in stickerbook.</returns>
    StickerData[] LoadPlacedStickers();
    /// <summary>
    /// Save data of sticker placements.
    /// </summary>
    /// <param name="stickerData">Data of stickers placed in stickerbook.</param>
    void SavePlacedStickers(StickerData[] stickerData);
    /// <summary>
    /// Loads current settings at the start of game.
    /// </summary>
    /// <returns></returns>
    MiscData LoadMiscData();
    /// <summary>
    /// Saves current settings.
    /// </summary>
    /// <param name="current"></param>
    void SaveMiscData(MiscData current);
    /// <summary>
    /// Resests all saved data.
    /// </summary>
    void ResetAllSavedData();
    /// <summary>
    /// Resets saved data of collected stickers.
    /// </summary>
    void ResetStickers();
    /// <summary>
    /// Resets save data of placed stickers.
    /// </summary>
    void ResetPlacedStickers();
    /// <summary>
    /// Resets settings.
    /// </summary>
    void ResetMiscData();
}

